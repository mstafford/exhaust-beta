from django.conf.urls import url, include
from django.contrib.gis import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
#from activities import views as act_views
from rest_framework import routers, serializers, viewsets

import notifications.urls

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
#router.register(r'actions', act_views.ActionList)
#router.register(r'types', TypeViewSet)
#router.register(r'users', act_views.UserList.as_view(), basename="users")
#router.register(r'groups', GroupViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include('activities.urls')),
#    url(r'^', include('charts.urls')),
    url(r'^maptest/', include('maptest.urls')),
    url('^inbox/notifications/', include(notifications.urls, namespace='notifications')),
    url(r'^keychain/', include('keychain.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
#    url(r'^login/$', auth_views.LoginView, name='login'),
#    url(r'^logout/$', auth_views.LogoutView, name='logout'),
    url(r'^markdownx/', include('markdownx.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^imagefit/', include('imagefit.urls')),
]
