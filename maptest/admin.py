from django.contrib.gis import admin
#General GIS objects
from .models import WorldBorder, Provinces

#Climbing map objects
from .models import CragBorder, CragWall, ClimbingRoute

# Register your models here.

admin.site.register(WorldBorder, admin.OSMGeoAdmin)
admin.site.register(Provinces, admin.OSMGeoAdmin)
admin.site.register(CragBorder, admin.OSMGeoAdmin)
admin.site.register(CragWall, admin.OSMGeoAdmin)
admin.site.register(ClimbingRoute)
