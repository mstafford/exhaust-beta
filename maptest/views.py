from maptest.forms import GeoForm, CragForm
# Generic Map Areas
from maptest.models import WorldBorder, Provinces

# EXHAUST Map Areas
from maptest.models import CragBorder, CragWall, ClimbingRoute

from django.shortcuts import render
from datetime import datetime
from django.contrib import messages as ms
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point, MultiPolygon, LineString
from django.contrib.gis.geoip2 import GeoIP2
from django.contrib.gis.geos.collections import GeometryCollection
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.views import View

import re

# Create your views here.

def world(request):
    countries = WorldBorder.objects.filter(pop2005__gte=0).order_by('name')
    g = GeoIP2()
    ipaddy = request.META.get('HTTP_X_REAL_IP', None)
    all_countries = 'SRID=4326;MULTIPOLYGON ('
    for idx, border in enumerate(countries):
        temp_text = re.sub('SRID=4326;MULTIPOLYGON \(','',border.mpoly.ewkt)
        temp_text = re.sub('\)\)\)','))',temp_text)
        if idx > 0:
            all_countries += ","
        all_countries += temp_text
    all_countries += ")"
    mpoly = MultiPolygon()
    mpoly = mpoly.from_ewkt(all_countries)
    geo = GeoForm()
    geo.fields['mpoly'].initial = mpoly
    context = {
    'map':geo,
    'countries': countries,
    'selected': {'name':"Earth"},
    }
    template = loader.get_template('maptest/map.html')

    return HttpResponse(template.render(context,request))

def country(request, country_id):
    country = WorldBorder.objects.get(pk=country_id)
    provinces = Provinces.objects.filter(centroid__within=country.mpoly).order_by('name')
    all_provs = 'SRID=4326;MULTIPOLYGON ('
    for idx, province in enumerate(provinces):
        temp_text = re.sub('SRID=4326;MULTIPOLYGON \(','',province.geom.ewkt)
        temp_text = re.sub('\)\)\)','))',temp_text)
        if idx > 0:
            all_provs += ","
        all_provs += temp_text
    all_provs += ")"
    mpoly = MultiPolygon()
    mpoly = mpoly.from_ewkt(all_provs)
    geo = GeoForm()
    geo.fields['mpoly'].initial = mpoly
    context = {
    'map':geo,
    'selected':country,
    'provinces':provinces,
    'countries': WorldBorder.objects.all().order_by('name'),
    }
    template = loader.get_template('maptest/map.html')
    return HttpResponse(template.render(context,request))

def province(request, country_id, province_id):
    province = Provinces.objects.get(pk=province_id)
    country = WorldBorder.objects.get(pk=country_id)
    provinces = Provinces.objects.filter(centroid__within=country.mpoly).order_by('name')
    crags = CragBorder.objects.filter(centroid__within=province.geom).order_by('name')
    all_crags = 'SRID=4326;MULTIPOLYGON ('
    if len(crags)>=1:
        for idx, crag in enumerate(crags):
            temp_text = re.sub('SRID=4326;MULTIPOLYGON \(','',crag.mpoly.ewkt)
            temp_text = re.sub('\)\)\)','))',temp_text)
            if idx > 0:
                all_crags += ","
            all_crags += temp_text
        all_crags += ")"
        mpoly = MultiPolygon()
        mpoly = mpoly.from_ewkt(all_crags)
    else:
        mpoly = province.geom

    geo = GeoForm()
    geo.fields['mpoly'].initial = mpoly
    context = {
    'map':geo,
    'selected':province,
    'provinces':provinces,
    'country':country,
    'crags':crags,
    }
    if request.method == "POST":
        return HttpResponse(request.POST['mpoly'])
    template = loader.get_template('maptest/map.html')
    return HttpResponse(template.render(context,request))

def crag(request, country_id, province_id, crag_id):
    crag = CragBorder.objects.get(pk=crag_id)
    province = Provinces.objects.get(pk=province_id)
    country = WorldBorder.objects.get(pk=country_id)
    crags = CragBorder.objects.filter(centroid__within=province.geom).order_by('name')
    walls = CragWall.objects.filter(wall_location__within=crag.mpoly).order_by('name')
    geo = GeoForm()
    geo.fields['mpoly'].initial = crag.mpoly
    template = loader.get_template('maptest/map.html')
    if request.method == 'POST':
        crag.mpoly = request.POST['mpoly']
        crag.save()
        return(HttpResponse(request.POST))
    context = {
    'map':geo,
    'selected':crag,
    'country':country,
    'province':province,
    'crags':crags,
    'walls':walls,
    'cragform':CragForm(initial={'name':crag.name}),
    }
    return HttpResponse(template.render(context,request))

def wall(request, country_id, province_id, crag_id, wall_id):
    wall = CragWall.objects.get(pk=wall_id)
    crag = CragBorder.objects.get(pk=crag_id)
    province = Provinces.objects.get(pk=province_id)
    country = WorldBorder.objects.get(pk=country_id)
    walls = CragWall.objects.filter(wall_location__within=crag.mpoly).order_by('name')
    routes = ClimbingRoute.objects.filter(cragwall=wall).order_by('name')
    geo = GeoForm()
    geo.fields['mpoly'].initial = wall.wall_location
    template = loader.get_template('maptest/map.html')
    if request.method == 'POST':
        wall.mpoly = request.POST['mpoly']
        wall.save()
        return(HttpResponse(request.POST))
    context = {
    'map':geo,
    'selected':wall,
    'country':country,
    'province':province,
    'crag':crag,
    'routes':routes,
    'walls':walls,
#    'cragform':CragForm(initial={'name':crag.name}),
    }
    return HttpResponse(template.render(context,request))

