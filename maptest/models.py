from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.contrib.gis.db import models

exhaust=User.objects.get(username="exhaust")

class WorldBorder(models.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    name = models.CharField(max_length=50)
    area = models.IntegerField()
    pop2005 = models.IntegerField('Population 2005')
    fips = models.CharField('FIPS Code', max_length=2)
    iso2 = models.CharField('2 Digit ISO', max_length=2)
    iso3 = models.CharField('3 Digit ISO', max_length=3)
    un = models.IntegerField('United Nations Code')
    region = models.IntegerField('Region Code')
    subregion = models.IntegerField('Sub-Region Code')
    lon = models.FloatField()
    lat = models.FloatField()

    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.MultiPolygonField()

    # Returns the string representation of the model.
    def __str__(self):
        return self.name

class Provinces(models.Model):       
    featurecla = models.CharField(max_length=20)
    scalerank = models.IntegerField()        
    adm1_code = models.CharField(max_length=9) 
    diss_me = models.IntegerField()
    iso_3166_2 = models.CharField(max_length=8)
    wikipedia = models.CharField(max_length=84)
    iso_a2 = models.CharField(max_length=2)    
    adm0_sr = models.IntegerField()          
    name = models.CharField(max_length=44)    
    name_alt = models.CharField(max_length=129)
    name_local = models.CharField(max_length=66)
    type = models.CharField(max_length=38)   
    type_en = models.CharField(max_length=27)
    code_local = models.CharField(max_length=5)
    code_hasc = models.CharField(max_length=8)
    note = models.CharField(max_length=114)  
    hasc_maybe = models.CharField(max_length=13)
    region = models.CharField(max_length=43) 
    region_cod = models.CharField(max_length=15)
    provnum_ne = models.IntegerField()       
    gadm_level = models.IntegerField()       
    check_me = models.IntegerField()         
    datarank = models.IntegerField()         
    abbrev = models.CharField(max_length=9)  
    postal = models.CharField(max_length=3)  
    area_sqkm = models.IntegerField()        
    sameascity = models.IntegerField()       
    labelrank = models.IntegerField()        
    name_len = models.IntegerField()
    mapcolor9 = models.IntegerField()         
    mapcolor13 = models.IntegerField()
    fips = models.CharField(max_length=5)
    fips_alt = models.CharField(max_length=9)                 
    woe_id = models.IntegerField()
    woe_label = models.CharField(max_length=64)
    woe_name = models.CharField(max_length=44)
    latitude = models.FloatField()
    longitude = models.FloatField()
    sov_a3 = models.CharField(max_length=3)
    adm0_a3 = models.CharField(max_length=3)
    adm0_label = models.IntegerField()
    admin = models.CharField(max_length=36)
    geonunit = models.CharField(max_length=40)
    gu_a3 = models.CharField(max_length=3)
    gn_id = models.IntegerField()
    gn_name = models.CharField(max_length=72)
    gns_id = models.IntegerField()
    gns_name = models.CharField(max_length=80)
    gn_level = models.IntegerField()
    gn_region = models.CharField(max_length=1)
    gn_a1_code = models.CharField(max_length=10)
    region_sub = models.CharField(max_length=41)
    sub_code = models.CharField(max_length=5)
    gns_level = models.IntegerField()
    gns_lang = models.CharField(max_length=3)
    gns_adm1 = models.CharField(max_length=4)
    gns_region = models.CharField(max_length=4)
    min_label = models.FloatField()
    max_label = models.FloatField()
    min_zoom = models.FloatField()
    wikidataid = models.CharField(max_length=9)
    name_ar = models.CharField(max_length=85)
    name_bn = models.CharField(max_length=128)
    name_de = models.CharField(max_length=51)
    name_en = models.CharField(max_length=47)
    name_es = models.CharField(max_length=52)
    name_fr = models.CharField(max_length=52)
    name_el = models.CharField(max_length=82)
    name_hi = models.CharField(max_length=134)
    name_hu = models.CharField(max_length=41)
    name_id = models.CharField(max_length=45)
    name_it = models.CharField(max_length=49)
    name_ja = models.CharField(max_length=93)
    name_ko = models.CharField(max_length=58)
    name_nl = models.CharField(max_length=44)
    name_pl = models.CharField(max_length=45)
    name_pt = models.CharField(max_length=43)
    name_ru = models.CharField(max_length=91)
    name_sv = models.CharField(max_length=48)
    name_tr = models.CharField(max_length=44)
    name_vi = models.CharField(max_length=49)
    name_zh = models.CharField(max_length=61)
    ne_id = models.BigIntegerField()
    geom = models.MultiPolygonField(srid=4326)
    centroid = models.PointField()

class CragBorder(models.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    name = models.CharField(max_length=50)
#    area = models.IntegerField()
    province  = models.ForeignKey(Provinces, related_name="province", on_delete=models.CASCADE)
#    lon = models.FloatField()
#    lat = models.FloatField()

    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.MultiPolygonField()
    centroid = models.PointField(default=Point(0, 0))
    outdoor = models.IntegerField(default=0)
    photo = models.ImageField(upload_to="documents/images/crags/", blank=True)
#    warden = models.ForeignKey(User,default=exhaust.pk, related_name="warden", on_delete=models.CASCADE)
    # Returns the string representation of the model.
    def __str__(self):
        return self.name

    def find_centroid(self):
        self.centroid = self.mpoly.centroid
        self.save()

class CragWall(models.Model):
    name = models.CharField(max_length=120)
    crag = models.ForeignKey(CragBorder, related_name="crag", on_delete=models.CASCADE)
    wall_location = models.LineStringField()
    photo = models.ImageField(upload_to="documents/images/crags/walls/", blank=True)

    def __str__(self):
        return self.crag.name + " - " +self.name

class ClimbingRoute(models.Model):
    STYLE_CHOICES = (
        (0, 'Single Trad'),
        (1, 'Multipitch Trad'),
        (2, 'Bigwall Trad'),
        (3, 'Single Sport'),
        (4, 'Multipitch Sport'),
        (5, 'Bigwall Sport'),
        (6, 'Bouldering'),
        (7, 'Aid Climbing'),
        (8, 'Free Solo'),
    )
    RATING_CHOICES = (
        (0, 'Standard'),
        (1, 'Worthwhile'),
        (2, 'Great Climb'),
        (3, 'Classic'),
        (4, 'Mega Classic'),
        (5, 'Mega Classic! Must Do!'),
    )

    name = models.CharField(max_length=120)
    cragwall = models.ForeignKey(CragWall, related_name="wall", on_delete=models.CASCADE)
    style = models.IntegerField(default=3, choices=STYLE_CHOICES)
    difficulty = models.CharField(max_length=10, blank=True)
    protection = models.CharField(max_length=256, blank=True)
    bolts = models.IntegerField(default=0, blank=True)
    height = models.IntegerField(default=0, blank=True)
    description = models.TextField(max_length=4096, blank=True)
    rating = models.IntegerField(default=0, choices=RATING_CHOICES)
    safety = models.CharField(max_length=5,blank=True)
    photo = models.ImageField(upload_to="documents/images/crags/walls/routes/", blank=True)


    def __str__(self):
       return self.cragwall.crag.name + " - " + self.cragwall.name + ": " + self.name
