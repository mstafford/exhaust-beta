from django.contrib.gis import admin
from django.urls import include, path

from . import views

urlpatterns = [
    path("", views.world, name="world"),
    path("c<int:country_id>/",views.country, name='country'),
    path("c<int:country_id>/p<int:province_id>/",views.province, name="province"),
    path("c<int:country_id>/p<int:province_id>/cb<int:crag_id>/",views.crag, name="crag"),
    path("c<int:country_id>/p<int:province_id>/cb<int:crag_id>/w<int:wall_id>/",views.wall, name="wall"),
]
