import os
from django.contrib.gis.utils import LayerMapping
from .models import WorldBorder, Provinces

world_mapping = {
    'fips' : 'FIPS',
    'iso2' : 'ISO2',
    'iso3' : 'ISO3',
    'un' : 'UN',
    'name' : 'NAME',
    'area' : 'AREA',
    'pop2005' : 'POP2005',
    'region' : 'REGION',
    'subregion' : 'SUBREGION',
    'lon' : 'LON',
    'lat' : 'LAT',
    'mpoly' : 'MULTIPOLYGON',
}

province_mapping = {
    'featurecla': 'featurecla',
    'scalerank': 'scalerank',
    'adm1_code': 'adm1_code',
    'diss_me': 'diss_me',
    'iso_3166_2': 'iso_3166_2',
    'wikipedia': 'wikipedia',
    'iso_a2': 'iso_a2',
    'adm0_sr': 'adm0_sr',
    'name': 'name',
    'name_alt': 'name_alt',
    'name_local': 'name_local',
    'type': 'type',
    'type_en': 'type_en',
    'code_local': 'code_local',
    'code_hasc': 'code_hasc',
    'note': 'note',
    'hasc_maybe': 'hasc_maybe',
    'region': 'region',
    'region_cod': 'region_cod',
    'provnum_ne': 'provnum_ne',
    'gadm_level': 'gadm_level',
    'check_me': 'check_me',
    'datarank': 'datarank',
    'abbrev': 'abbrev',
    'postal': 'postal',
    'area_sqkm': 'area_sqkm',
    'sameascity': 'sameascity',
    'labelrank': 'labelrank',
    'name_len': 'name_len',
    'mapcolor9': 'mapcolor9',
    'mapcolor13': 'mapcolor13',
    'fips': 'fips',
    'fips_alt': 'fips_alt',
    'woe_id': 'woe_id',
    'woe_label': 'woe_label',
    'woe_name': 'woe_name',
    'latitude': 'latitude',
    'longitude': 'longitude',
    'sov_a3': 'sov_a3',
    'adm0_a3': 'adm0_a3',
    'adm0_label': 'adm0_label',
    'admin': 'admin',
    'geonunit': 'geonunit',
    'gu_a3': 'gu_a3',
    'gn_id': 'gn_id',
    'gn_name': 'gn_name',
    'gns_id': 'gns_id',
    'gns_name': 'gns_name',
    'gn_level': 'gn_level',
    'gn_region': 'gn_region',
    'gn_a1_code': 'gn_a1_code',
    'region_sub': 'region_sub',
    'sub_code': 'sub_code',
    'gns_level': 'gns_level',
    'gns_lang': 'gns_lang',
    'gns_adm1': 'gns_adm1',
    'gns_region': 'gns_region',
    'min_label': 'min_label',
    'max_label': 'max_label',
    'min_zoom': 'min_zoom',
    'wikidataid': 'wikidataid',
    'name_ar': 'name_ar',
    'name_bn': 'name_bn',
    'name_de': 'name_de',
    'name_en': 'name_en',
    'name_es': 'name_es',
    'name_fr': 'name_fr',
    'name_el': 'name_el',
    'name_hi': 'name_hi',
    'name_hu': 'name_hu',
    'name_id': 'name_id',
    'name_it': 'name_it',
    'name_ja': 'name_ja',
    'name_ko': 'name_ko',
    'name_nl': 'name_nl',
    'name_pl': 'name_pl',
    'name_pt': 'name_pt',
    'name_ru': 'name_ru',
    'name_sv': 'name_sv',
    'name_tr': 'name_tr',
    'name_vi': 'name_vi',
    'name_zh': 'name_zh',
    'ne_id': 'ne_id',
    'geom': 'MULTIPOLYGON',
}

world_shp = os.path.abspath(
    os.path.join(os.path.dirname(__file__), 'data', 'TM_WORLD_BORDERS-0.3.shp'),
)

province_shp = os.path.abspath(
    os.path.join(os.path.dirname(__file__), 'data', 'ne_10m_admin_1_states_provinces.shp'),
)


def load_countries(verbose=True):
    lm = LayerMapping(WorldBorder, world_shp, world_mapping, transform=False)
    lm.save(strict=True, verbose=verbose)


def load_provinces(verbose=True):
    lm = LayerMapping(Provinces, province_shp, province_mapping, transform=False)
    lm.save(strict=True, verbose=verbose)

