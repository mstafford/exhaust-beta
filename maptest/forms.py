from maptest.models import WorldBorder
from django.contrib.gis import forms

class CragForm(forms.Form):
    name = forms.CharField(min_length=3, max_length=50, required = True, label="Crag Name", initial="Name", help_text="50 Characters Max!")

class GeoForm(forms.Form):
#    point = forms.PointField(widget=
    label="Test"
    mpoly = forms.MultiPolygonField(widget=
       forms.OSMWidget(attrs={
           'map_width': 500,
           'default_zoom':15,
#           'map_height': 500
       })
    )

#class TestForm(forms.ModelForm):
#    class Meta:
#        model = WorldBorder
