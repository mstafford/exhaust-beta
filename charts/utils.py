from activities.models import Stat
from datetime import datetime

import ast
import tcxparser
import json

def json_chart(json_data):
    prev_dist = 0
    prev_time = 0
    now_dist = 0
    now_time = 0
    delta_dist = 0
    delta_time = 0
    inst_pace = 0 ## min/km
    inst_speed = 0 ## km/hr
    streams = ast.literal_eval(json_data)
    chart_json = []
    splits = []
    stamp_time = 0
    stamp_dist = 0
    stamp_elev = 0
    for idx, point in enumerate(streams[0]['data']):
        print(point)
        for stream in streams: #Load relevant info
            if stream['type']=='time':
                stamp_time = stream['data'][idx]
            if stream['type']=='distance':
                stamp_dist = stream['data'][idx]
                if int(stream['data'][idx]/1000) > len(splits):
                    try:
                        splits.append({'split':'km '+str(len(splits)),'totaltime':stamp_time,'splittime':(stamp_time-splits[len(splits)-1]['totaltime'])/60})
                    except:
                        splits.append({'split':'km '+str(len(splits)),'totaltime':stamp_time,'splittime':stamp_time/60})
            if stream['type']=='altitude':
                stamp_elev = stream['data'][idx]
        if idx == 0:
            chart_json.append({"distance":round(stamp_dist/1000,2),"elevation":stamp_elev,"pace":0,"speed":0})
            prev_dist = stamp_dist
            prev_time = stamp_time
        else:
            now_dist = stamp_dist
            now_time = stamp_time
            delta_dist = (now_dist + 0.2 - prev_dist)/1000 # elapsed distance in km's
            delta_time = (now_time - prev_time)/60 # elapsed time in minutes
            inst_pace = round(delta_time/delta_dist,2) # instantaneous pace in min/km
            try:
                inst_speed = round(delta_dist/(delta_time/60),2) # instantaneous speed in km/hr
            except ZeroDivisionError:
                inst_speed = 0
            chart_json.append({"distance":round(stamp_dist/1000,2),"elevation":stamp_elev,"pace":inst_pace,"speed":inst_speed})
            prev_dist = stamp_dist
            prev_time = stamp_time
    return(chart_json, splits)

def gpx_chart(points):
    prev_dist = 0
    prev_time = 0
    now_dist = 0
    now_time = 0
    delta_dist = 0
    delta_time = 0
    inst_pace = 0 ## min/km
    inst_speed = 0 ## km/hr
    chart_json = []
    splits = []
    starttime = 0
    for idx, point in enumerate(points):
        if int(point.distance_from_start/1000) > len(splits):
            try:
                splits.append({'split':'km '+str(len(splits)),'totaltime': point[0].time.timestamp(),'splittime':(point[0].time.timestamp()-splits[len(splits)-1]['totaltime'])/60})
            except:
                splits.append({'split':'km '+str(len(splits)),'totaltime': point[0].time.timestamp(),'splittime':(point[0].time.timestamp()-starttime)/60})
        if idx == 0:
            starttime = point[0].time.timestamp()
            chart_json.append({"distance":round(point.distance_from_start/1000,2),"elevation":point[0].elevation,"pace":0,"speed":0})
            prev_dist = point.distance_from_start
            prev_time = point[0].time.timestamp()
        elif idx%10 == 0:
            now_dist = point.distance_from_start
            now_time = point[0].time.timestamp()
            delta_dist = (now_dist + 0.2 - prev_dist)/1000 # elapsed distance in km's
            delta_time = (now_time - prev_time)/60 # elapsed time in minutes
            inst_pace = round(delta_time/delta_dist,2) # instantaneous pace in min/km
            inst_speed = round(delta_dist/(delta_time/60),2) # instantaneous speed in km/hr
            chart_json.append({"distance":round(point.distance_from_start/1000,2),"elevation":point[0].elevation,"pace":inst_pace,"speed":inst_speed})
            prev_dist = point.distance_from_start
            prev_time = point[0].time.timestamp()
    return(chart_json, splits)

def gpx_map(points):
    map_json = []
    for point in points:
        map_json.append({"latitude":point[0].latitude,"longitude":point[0].longitude})
    return([json.dumps(map_json),json.dumps({"latitude":points[0][0].latitude,"longitude":points[0][0].longitude})])

def tcx_map(filename):
    tcx = tcxparser.TCXParser(filename)
    counter = 0
    map_json = []
    for lap in tcx.activity.Lap:
        for point in lap.Track.Trackpoint:
            if counter%10==0:
                try:
                    map_json.append({"latitude":str(point.Position.LatitudeDegrees),"longitude":str(point.Position.LongitudeDegrees)})
                except:
                    map_json.append({"latitude":str((counter/180)),"longitude":str((counter/180))})
            counter += 1
    try:
        return([json.dumps(map_json),json.dumps({"latitude":float(point.Position.LatitudeDegrees),"longitude":float(point.Position.LongitudeDegrees)})])
    except:
        return(['null','null'])

def tcx_chart(points):
    prev_dist = 0
    prev_time = 0
    now_dist = 0
    now_time = 0
    delta_dist = 0
    delta_time = 0
    inst_pace = 0 ## min/km
    inst_speed = 0 ## km/hr
    chart_json = []
    splits = []
    starttime = 0
    for idx, point in enumerate(points):
        if int(point[0]/1000) > len(splits):
            try:
                splits.append({'split':'km '+str(len(splits)),'totaltime': point[1],'splittime':(point[1]-splits[len(splits)-1]['totaltime'])/60})
            except:
                splits.append({'split':'km '+str(len(splits)),'totaltime': point[1],'splittime':(point[1]-starttime)/60})
        if idx == 0:
            chart_json.append({"distance":round(point[0]/1000,2),"elevation":point[2],"pace":0,"speed":0})
            starttime = point[1]
            prev_dist = point[0]
            prev_time = point[1]
        elif point[1] == prev_time:
            continue
        elif idx%5 == 0:
            now_dist = point[0]
            now_time = point[1]
            delta_dist = (now_dist + 0.1 - prev_dist)/1000 # elapsed distance in km's
            delta_time = (now_time - prev_time)/60 # elapsed time in minutes
            inst_pace = round(delta_time/delta_dist,2) # instantaneous pace in min/km
            inst_speed = round(delta_dist/(delta_time/60),2) # instantaneous speed in km/hr
            chart_json.append({"distance":round(now_dist/1000,2),"elevation":point[2],"pace":inst_pace,"speed":inst_speed})
            prev_dist = now_dist
            prev_time = now_time
    return(chart_json, splits)

def leader_chart(acttype, timeframe):
    chart_json = []
    stats = ''
    avatar = "/media/avatar_default.png"
    if timeframe == "WTDdist":
        stats = Stat.objects.filter(atype = acttype).order_by('WTDdist').exclude(WTDdist=0)
        for stat in stats:
            try:
                avatar = stat.user.userprofile.avatar
            except:
                avatar = "/media/avatar_default.png"
            chart_json.append({"username": stat.user.username, "distance":stat.WTDdist/1000, "href":avatar})
    if timeframe == "MTDdist":
        stats = Stat.objects.filter(atype = acttype).order_by('MTDdist').exclude(MTDdist=0)
        for stat in stats:
            try:
                avatar = stat.user.userprofile.avatar
            except:
                avatar = "/media/avatar_default.png"
            chart_json.append({"username": stat.user.username, "distance":stat.MTDdist/1000, "href":avatar})
    if timeframe == "YTDdist":
        stats = Stat.objects.filter(atype = acttype).order_by('YTDdist').exclude(YTDdist=0)
        for stat in stats:
            try:
                avatar = stat.user.userprofile.avatar
            except:
                avatar = "/media/avatar_default.png"
            chart_json.append({"username": stat.user.username, "distance":stat.YTDdist/1000, "href":avatar})
    if timeframe == "ALLdist":
        stats = Stat.objects.filter(atype = acttype).order_by('ALLdist').exclude(ALLdist=0)
        for stat in stats:
            try:
                avatar = stat.user.userprofile.avatar
            except:
                avatar = "/media/avatar_default.png"
            chart_json.append({"username": stat.user.username, "distance":stat.ALLdist/1000, "href":avatar})
    return(chart_json)
