import gpxpy
import gpxpy.gpx
import utm
import math
from datetime import datetime
import tcxparser
import re
import xml.etree.ElementTree as ET

point_list = []
lengthTotal = 0.0
runDuration = 0.0

#class dataScrape(self, filename):
#Import file function
#def importGPX(filename):
#    gpx_file = open(filename,'r')
#    gpx = gpxpy.parse(gpx_file)
#    #Load point list latitude and longitude from file
#    for track in gpx.tracks:
#        for segment in track.segments:
#            for point in segment.points:
#                timestamp = datetime.timestamp(point.time)
#                point_list.append([point.latitude,point.longitude,timestamp])
#    print(point_list[-1])
#    return(point_list)

def importGPX(filename):
    gpx_file = open(filename, 'r')
    gpx = gpxpy.parse(gpx_file)
    distance = gpx.length_2d()
    duration = gpx.get_duration()
    pace = round((duration/60)/(distance/1000),2)
    timeList = []
    splitList = []
    splitID = 1
    startTime = gpx.get_time_bounds().start_time.timestamp()
    counter = 1
    for point in gpx.get_points_data():
        if point.distance_from_start >= splitID * 1000:
            timeList.append([splitID, (point.point.time.timestamp()-startTime)])
            splitID += 1
    if len(timeList)==0:
        timeList.append([splitID,(point.point.time.timestamp()-startTime)])
        splitList.append([0,0])
    else:
        splitList.append([timeList[0][0],timeList[0][1]/60])
    for i in range(1,len(timeList)):
        splitList.append([timeList[i][0],(timeList[i][1]-timeList[i-1][1])/60])
#    return(distance,duration)
    return(distance, duration, pace, gpx.get_points_data(), gpx.get_time_bounds().start_time, splitList, gpx.description)

def parseXML(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    track=''
    counter = 0
    XMLpoints = []
    XMLpoints.clear()
    pointTime = 'NULL'
    distance = 0.0
    elevation = 'NULL'
    hr = 'NULL'
    cadence = 'NULL'
    wattage = 'NULL'
    speed = 'NULL'
    x1 = 0
    y1 = 0
    x2 = 0
    y2 = 0
    zone = "NULL"
    letter = "NULL"
    activity = "NULL"
    for child in root:
        if "Activities" in child.tag:
            activity=child[0]
    for child in activity:
        if "Lap" not in child.tag:
            continue
        else:
            Lap = child
        for lap in Lap:
            if "Track" in lap.tag:
                track = lap
            else:
                continue
            firstpoint = True
            for trackpoint in track:
                for child in trackpoint:
                    if "Time" in child.tag:
                        try:
                            pointTime= re.sub("[-+]\S{2}:\S{2}",'Z',str(child.text))
                            pointTime = datetime.timestamp(datetime.strptime(pointTime,"%Y-%m-%dT%H:%M:%SZ"))
                        except ValueError:
                            pointTime= re.sub("[-+]\S{2}:\S{2}",'Z',str(child.text))
                            pointTime = datetime.timestamp(datetime.strptime(pointTime,"%Y-%m-%dT%H:%M:%S.%fZ"))
                    if "Altitude" in child.tag:
                        elevation = round(float(child.text),0)
                    if "Distance" in child.tag:
                        distance=(float(child.text))
                    if "Position" in child.tag:
                        lat = child[0]
                        lon = child[1]
                        if firstpoint:
                            x1,y1,zone,letter = utm.conversion.from_latlon(float(lat.text),float(lon.text))
                            firstpoint = False
                        x2,y2,zone,letter = utm.conversion.from_latlon(float(lat.text),float(lon.text))
                        delX = x2 - x1
                        delY = y2 - y1
##                        print(x1,y1,x2,y2)
                        delta = math.sqrt(delY*delY + delX*delX)
                        distance += delta
##                        print(delta)
##                        print("=================")
                        x1 = x2
                        y1 = y2
                    if "Heart" in child.tag:
                        hr = int(child[0].text)
                    if "Cadence" in child.tag:
                        cadence = int(child.text)
                    if "Extensions" in child.tag:
                        for i in range(0,len(child[0])):
                            if "Watt" in child[0][i].tag:
                                wattage = int(child[0][i].text)
                            if "Speed" in child[0][i].tag:
                                speed = float(child[0][i].text)
                            if "Cadence" in child[0][i].tag:
                                cadence = int(child[0][i].text)
                    counter += 1
                XMLpoints.append([distance, pointTime, elevation, hr, cadence, wattage, speed])
    return(XMLpoints)



def importTCX(filename):
    tcx = tcxparser.TCXParser(filename)
    splitList = []
    point_list = []
    counter = 1
    for lap in tcx.activity.Lap:
        point_list = parseXML(filename)
        distance = lap.DistanceMeters/1000
        duration = lap.TotalTimeSeconds/60
        pace = round(duration/distance,2)
        if len(tcx.activity.Lap) > 1:
            if lap.DistanceMeters >= 1:
                splitList.append([counter,pace])
                counter +=1
            else:
                splitList.append([round(distance,2),pace])
        else:
            prevtime = 0.0
            counter = 0
            try:
                finishtime= re.sub("[-+]\d{2}:\d{2}",'Z',str(tcx.completed_at))
                finishtime = datetime.strptime(finishtime,'%Y-%m-%dT%H:%M:%SZ').timestamp()
            except:
                finishtime= re.sub("[-+]\d{2}:\d{2}",'Z',str(tcx.completed_at))
                finishtime = datetime.strptime(finishtime,'%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
            for i in range(1, int(distance) + 2):
                try:
                    starttime= re.sub("[-+]\d{2}:\d{2}",'Z',str(tcx.activity.Lap.Track.Trackpoint.Time))
                    starttime = datetime.strptime(starttime,'%Y-%m-%dT%H:%M:%SZ').timestamp()
                except:
                    starttime= re.sub("[-+]\d{2}:\d{2}",'Z',str(tcx.activity.Lap.Track.Trackpoint.Time))
                    starttime = datetime.strptime(starttime,'%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
                for point in lap.Track.Trackpoint:
                    counter += 1
                    try:
                        if int(point.DistanceMeters / 1000) >= i:
                            try:
                                time= re.sub("[-+]\d{2}:\d{2}",'Z',str(point.Time))
                                time = datetime.strptime(time,'%Y-%m-%dT%H:%M:%SZ').timestamp()
                            except:
                                time= re.sub("[-+]\d{2}:\d{2}",'Z',str(point.Time))
                                time = datetime.strptime(time,'%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
                            delta = time-starttime
                            split = delta/60 - prevtime
                            splitList.append([i,split])
                            prevtime += delta/60 - prevtime
                            break
                    except AttributeError:
                        pass
                    if point.Time == tcx.completed_at:
                        try:
                            time= re.sub("[-+]\d{2}:\d{2}",'Z',str(point.Time))
                            time = datetime.strptime(time,'%Y-%m-%dT%H:%M:%SZ').timestamp()
                        except:
                            time= re.sub("[-+]\d{2}:\d{2}",'Z',str(point.Time))
                            time = datetime.strptime(time,'%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
                        delta = time-starttime
                        split = (delta/60 - prevtime)/(int(tcx.distance/1000))
                        splitList.append([i,split])
                        prevtime += delta/60 - prevtime
    try:
        return(point_list, tcx.pace, tcx.hr_avg, tcx.hr_max, tcx.ascent, re.sub('T.{5,8}Z','',tcx.completed_at), tcx.distance, tcx.duration, splitList, tcx.activity_notes)
    except ZeroDivisionError:
        return(point_list, tcx.pace, 'NO HR DATA', 'NO HR DATA', tcx.ascent, re.sub('T.{5,8}Z','',tcx.completed_at), tcx.distance, tcx.duration, splitList, tcx.activity_notes)

#Convert points from Latitude and Longitude (DD.dddd) to UTM (E, N, ZONE, LETTER)
def convertUTM(points):
    for point in points:
        converted = utm.from_latlon(point[0],point[1])
        point[0], point[1] = converted[0], converted[1]

def runLength(points):
    lengthTotal = 0.0
    for i in range(1,len(points)):
        deltaX = abs(points[i][0] - points[i-1][0])
        deltaY = abs(points[i][1] - points[i-1][1])
        delta = math.sqrt((deltaX*deltaX)+(deltaY*deltaY))
        lengthTotal += delta
    lengthTotal = round(lengthTotal)
    return(lengthTotal)

def runTime(points):
    secondsDelta = points[-1][2] - points[0][2]
    return(secondsDelta)

def runPace(distance, duration, points):
    ### Tighten this shit up bigtime with datetime stuff. The below is fucking brutal
    # Pace in m/s
    pace = distance / duration
    # Pace in min/km
    minPace = 1000/pace/60
    secPace = round(1000/pace%60)
    pace = round(pace,2)
    minPace = round(minPace)
##    print(str(minPace)+":"+str(secPace)+"/km")
    
#importGPX(filename)
#convertUTM(point_list)
#lengthTotal = runLength(point_list)
#runDuration = runTime(point_list)
#print(lengthTotal, runDuration)
#runPace(lengthTotal, runDuration, point_list)

