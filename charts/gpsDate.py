import tcxparser
import gpxpy
import gpxpy.gpx
from activities.models import Activity
from datetime import datetime


def update_dates():
    now = datetime.now().timestamp()
    oneweek = datetime.fromtimestamp(now-24*3600*7)
    twoweek = datetime.fromtimestamp(now-24*3600*7*2)
    allacts = Activity.objects.filter(posted=True)
    for act in allacts:
#        print("=======================")
#        print(act.uploaded_at, act.user.username, act.pk)
        if act.gpsfile:
            if 'tcx' in act.gpsfile.name:
                tcx = tcxparser.TCXParser('media/'+act.gpsfile.name)
                try:
                    act.date = datetime.strptime(tcx.completed_at,'%Y-%m-%dT%H:%M:%SZ')
                    act.save()
                except:
                    try:
                        act.date = datetime.strptime(tcx.completed_at,'%Y-%m-%dT%H:%M:%S.%fZ')
                        act.save()
                    except:
                        try:
                            act.date = datetime.strptime(tcx.completed_at,'%Y-%m-%dT%H:%M:%S.%f-08:00')
                            act.save()
                        except:
                            act.date = datetime.strptime(tcx.completed_at,'%Y-%m-%dT%H:%M:%S.%f+00:00')
                            act.save()
            elif 'gpx' in act.gpsfile.name:
                gpxfile = open('media/'+act.gpsfile.name,'r')
                gpx = gpxpy.parse(gpxfile)
                print(act.title, act.distance, gpx.time)
                print(type(gpx.time))
                act.date = gpx.get_time_bounds()[0]
                act.save()
                gpxfile.close()
        else:
            print(act.title, act.uploaded_at)
            act.date = act.uploaded_at
            print("No GPS Data -- using upload date!")
            act.save()
