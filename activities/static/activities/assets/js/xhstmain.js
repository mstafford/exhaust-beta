// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue =   decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

// Toggler for Manual New Activity Method Selection:
$('#act_delete').on('click', function(event){
    event.preventDefault();
    console.log("Activity Deletion Request Detected!!");  // sanity check
    $.ajax({
        url : "/delete/", // the endpoint
        type : "POST", // http method
        data : { activity : $('#activity_id').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
            window.location.href = "https://xhaust.me/upload/";
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });

});
// Toggler for Manual New Activity Method Selection:
$('#act_update').on('click', function(event){
    event.preventDefault();
    console.log("Activity Update Request Detected!!");  // sanity check
});

// Toggler for Manual New Activity Method Selection:
$('#act_preview').on('click', function(event){
    event.preventDefault();
    console.log("Post Preview Request Detected!!");  // sanity check
});


// Update vote on slider timeout:
$('#commenter').on('change', function(event){
    event.preventDefault();
    console.log("Comment Input Detected!")  // sanity check
    var text = $('#id_commentbody').val();
//    var regex = /\r?\n|\r/;
//    text = text.replace(regex, "\n");
    $("#comment_body").val(text);
//    update_comment_body();
});

// SliderForms

// Update vote on slider timeout:
$('#upvote-slider').on('change', function(event){
    event.preventDefault();
    console.log("Change Detected!")  // sanity check
    get_upvote_value();
});

// AJAX for grabbing vote weight
function get_upvote_value() {
    console.log("Grabbing vote numbers") // sanity check
    console.log($('#id_upvote').val());
    $.ajax({
        url : "/votecalc/", // the endpoint
        type : "POST", // http method
        data : { weight : $('#id_upvote').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#upweight").text(json.weight+"% VOTE = ");
            $("#upvalue").text(json.weightvalue+" SBD");
            $("#upvote_weight").val(json.weight*100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

// Toggler for Manual New Activity Method Selection:
$('#togglemanual').on('click', function(event){
    event.preventDefault();
    console.log("Manual Data Entry Selected!");  // sanity check
    document.querySelector("#manualentry").style.display="flex";
    document.querySelector("#gpsentry").style.display="none";
    document.querySelector("#acctsync").style.display="none";
});

// Toggler for Manual New Activity Method Selection:
$('#togglefile').on('click', function(event){
    event.preventDefault();
    console.log("GPS Upload Selected!");  // sanity check
    document.querySelector("#manualentry").style.display="none";
    document.querySelector("#gpsentry").style.display="flex";
    document.querySelector("#acctsync").style.display="none";
});

// Toggler for Manual New Activity Method Selection:
$('#togglesync').on('click', function(event){
    event.preventDefault();
    console.log("AccountSync Selected!");  // sanity check
    document.querySelector("#manualentry").style.display="none";
    document.querySelector("#gpsentry").style.display="none";
    document.querySelector("#acctsync").style.display="flex";
});

// Toggler for Manual Running Chart Selection:
$('#running_charts').on('click', function(event){
    event.preventDefault();
    document.querySelector("#runcharts").style.display="block";
    document.querySelector("#cyclecharts").style.display="none";
    document.querySelector("#hikecharts").style.display="none";
});
// Toggler for Manual Cycling Chart Selection:
$('#cycling_charts').on('click', function(event){
    event.preventDefault();
    document.querySelector("#runcharts").style.display="none";
    document.querySelector("#cyclecharts").style.display="block";
    document.querySelector("#hikecharts").style.display="none";
});
// Toggler for Manual Hiking Chart Selection:
$('#hiking_charts').on('click', function(event){
    event.preventDefault();
    document.querySelector("#runcharts").style.display="none";
    document.querySelector("#cyclecharts").style.display="none";
    document.querySelector("#hikecharts").style.display="block";
});

// Toggler for Manual Hiking Chart Selection:
$('#weekly_charts').on('click', function(event){
    event.preventDefault();
    document.querySelector("#runWTD").style.display="block";
    document.querySelector("#cycleWTD").style.display="block";
    document.querySelector("#hikeWTD").style.display="block";
    document.querySelector("#runMTD").style.display="none";
    document.querySelector("#cycleMTD").style.display="none";
    document.querySelector("#hikeMTD").style.display="none";
    document.querySelector("#runYTD").style.display="none";
    document.querySelector("#cycleYTD").style.display="none";
    document.querySelector("#hikeYTD").style.display="none";
    document.querySelector("#runALL").style.display="none";
    document.querySelector("#cycleALL").style.display="none";
    document.querySelector("#hikeALL").style.display="none";
});

// Toggler for Manual Hiking Chart Selection:
$('#monthly_charts').on('click', function(event){
    event.preventDefault();
    document.querySelector("#runWTD").style.display="none";
    document.querySelector("#cycleWTD").style.display="none";
    document.querySelector("#hikeWTD").style.display="none";
    document.querySelector("#runMTD").style.display="block";
    document.querySelector("#cycleMTD").style.display="block";
    document.querySelector("#hikeMTD").style.display="block";
    document.querySelector("#runYTD").style.display="none";
    document.querySelector("#cycleYTD").style.display="none";
    document.querySelector("#hikeYTD").style.display="none";
    document.querySelector("#runALL").style.display="none";
    document.querySelector("#cycleALL").style.display="none";
    document.querySelector("#hikeALL").style.display="none";
});

// Toggler for Manual Hiking Chart Selection:
$('#yearly_charts').on('click', function(event){
    event.preventDefault();
    document.querySelector("#runWTD").style.display="none";
    document.querySelector("#cycleWTD").style.display="none";
    document.querySelector("#hikeWTD").style.display="none";
    document.querySelector("#runMTD").style.display="none";
    document.querySelector("#cycleMTD").style.display="none";
    document.querySelector("#hikeMTD").style.display="none";
    document.querySelector("#runYTD").style.display="block";
    document.querySelector("#cycleYTD").style.display="block";
    document.querySelector("#hikeYTD").style.display="block";
    document.querySelector("#runALL").style.display="none";
    document.querySelector("#cycleALL").style.display="none";
    document.querySelector("#hikeALL").style.display="none";
});

// Toggler for Manual Hiking Chart Selection:
$('#alltime_charts').on('click', function(event){
    event.preventDefault();
    document.querySelector("#runWTD").style.display="none";
    document.querySelector("#cycleWTD").style.display="none";
    document.querySelector("#hikeWTD").style.display="none";
    document.querySelector("#runMTD").style.display="none";
    document.querySelector("#cycleMTD").style.display="none";
    document.querySelector("#hikeMTD").style.display="none";
    document.querySelector("#runYTD").style.display="none";
    document.querySelector("#cycleYTD").style.display="none";
    document.querySelector("#hikeYTD").style.display="none";
    document.querySelector("#runALL").style.display="block";
    document.querySelector("#cycleALL").style.display="block";
    document.querySelector("#hikeALL").style.display="block";
});


// Togglers for FEED page:
$('#feed_toggle_all').on('click', function(event){
    event.preventDefault();
    console.log('TOGGLE ALL');
    document.querySelector('#feed_content_all').style.display="flex";
    document.querySelector('#feed_content_following').style.display="none";
});

$('#feed_toggle_following').on('click', function(event){
    event.preventDefault();
    console.log('TEGGLE FOLLOWING');
    document.querySelector('#feed_content_all').style.display="none";
    document.querySelector('#feed_content_following').style.display="flex";
});


// Togglers for Dashboard Pages:
// Toggler for Manual Hiking Chart Selection:
$('#dash_recent_all').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_main_recent").style.display="flex";
    document.querySelector("#dash_cycling_stats").style.display="none";
    document.querySelector("#dash_running_stats").style.display="none";
    document.querySelector("#dash_hiking_stats").style.display="none";
});

$('#dash_recent_running').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_main_recent").style.display="none";
    document.querySelector("#dash_cycling_stats").style.display="none";
    document.querySelector("#dash_running_stats").style.display="flex";
    document.querySelector("#dash_hiking_stats").style.display="none";

});
$('#dash_recent_cycling').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_main_recent").style.display="none";
    document.querySelector("#dash_cycling_stats").style.display="flex";
    document.querySelector("#dash_running_stats").style.display="none";
    document.querySelector("#dash_hiking_stats").style.display="none";
});
$('#dash_recent_hiking').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_main_recent").style.display="none";
    document.querySelector("#dash_cycling_stats").style.display="none";
    document.querySelector("#dash_running_stats").style.display="none";
    document.querySelector("#dash_hiking_stats").style.display="flex";
});
// Togglers for reports activity selection
// AJAX for grabbing vote weight
function get_report_contents() {
    console.log("Grabbing recent activities") // sanity check
//    console.log($('#id_upvote').val());
    $.ajax({
        url : "/report/", // the endpoin
        type : "POST", // http method
        data : {
            activitytype : $('#activitytype').val(),
            timeframe :$('#timeframe').val(),
         }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#id_post_body").val(json.reportbody);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {   
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

$('#dash_report_main').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_info_reports").style.display="flex";
    document.querySelector("#dash_main_reports").style.display="none";
});
$('#dash_report_running').on('click', function(event){
    event.preventDefault();
    $('#activitytype').val("running");
    $('#timeframe').val("ninetyday");
    get_report_contents();
    document.querySelector("#dash_info_reports").style.display="none";
    document.querySelector("#dash_main_reports").style.display="flex";
});
$('#dash_report_cycling').on('click', function(event){
    event.preventDefault();
    $("#activitytype").val('cycling');
    $('#timeframe').val('ninetyday');
    get_report_contents();
    document.querySelector("#dash_info_reports").style.display="none";
    document.querySelector("#dash_main_reports").style.display="flex";
});
$('#dash_report_hiking').on('click', function(event){
    event.preventDefault();
    $("#activitytype").val('hiking');
    $('#timeframe').val('ninetyday');
    get_report_contents();
    document.querySelector("#dash_info_reports").style.display="none";
    document.querySelector("#dash_main_reports").style.display="flex";
});



$('#dash_toggle_recent').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_content_recent").style.display="flex";
    document.querySelector("#dash_content_settings").style.display="none";
    document.querySelector("#dash_content_report").style.display="none";
});

$('#dash_toggle_settings').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_content_recent").style.display="none";
    document.querySelector("#dash_content_settings").style.display="flex";
    document.querySelector("#dash_content_report").style.display="none";
});

$('#dash_toggle_reports').on('click', function(event){
    event.preventDefault();
    document.querySelector("#dash_content_recent").style.display="none";
    document.querySelector("#dash_content_settings").style.display="none";
    document.querySelector("#dash_content_report").style.display="flex";
});


// Update vote on slider timeout:
$('#dashSummary').on('click', function(event){
    event.preventDefault();
    console.log("Dashboard Summary Requested!");  // sanity check
    document.querySelector("#sidepanelSummary").style.display="block";
    document.querySelector("#summaryFeed").style.display="block";
    document.querySelector("#sidepanelYears").style.display="none";
    document.querySelector("#activityDetail").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="none";
    document.querySelector("#xhstSettings").style.display="none";
});

// Update vote on slider timeout:
$('#dashActivities').on('click', function(event){
    event.preventDefault();
    console.log("Dashboard Activities Requested!")  // sanity check
    document.querySelector("#sidepanelSummary").style.display="none";
    document.querySelector("#summaryFeed").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="none";
    document.querySelector("#xhstSettings").style.display="none";
    document.querySelector("#sidepanelYears").style.display="block";
    document.querySelector("#activityDetail").style.display="block";
});

// Display and Edit Users XHST Settings (API Key)
$('#dashSettings').on('click', function(event){
    event.preventDefault();
    console.log("User Settings Requested!") // sanity check
    document.querySelector("#sidepanelSummary").style.display="none";
    document.querySelector("#summaryFeed").style.display="none";
    document.querySelector("#sidepanelYears").style.display="none";
    document.querySelector("#activityDetail").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="block";
    document.querySelector("#xhstSettings").style.display="block";
    get_user_settings();
});

// Toggle different tabs in settings page
$('#dash_accounts_toggle').on('click', function(event){
    event.preventDefault();
    console.log("Prefences") // sanity check  
    document.querySelector("#dash_settings_preferences").style.display="none";
    document.querySelector("#dash_settings_accounts").style.display="block";
    get_user_settings();
});

$('#dash_preferences_toggle').on('click', function(event){
    event.preventDefault();
    console.log("Prefences") // sanity check  
    document.querySelector("#dash_settings_preferences").style.display="block";
    document.querySelector("#dash_settings_accounts").style.display="none";
    get_user_settings();
});


// AJAX for grabbing vote weight
function get_user_settings() {
    console.log("Retrieving Settings") // sanity check
    $.ajax({
        url : "/usersettings/", // the endpoint
        type : "GET", // http method
        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#xhstAPIKey").text(json.apikey);
            $("#xhstMaps").text(json.map_privacy);
//            $("#downvote_weight").val(json.weight*-100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};



// Update vote on slider timeout:
$('#downvote-slider').on('change', function(event){
    event.preventDefault();
    console.log("Change Detected!")  // sanity check
    get_downvote_value();
});

// AJAX for grabbing vote weight
function get_downvote_value() {
    console.log("Grabbing vote numbers") // sanity check
    console.log($('#id_downvote').val());
    $.ajax({
        url : "/votecalc/", // the endpoint
        type : "POST", // http method
        data : { weight : $('#id_downvote').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#downweight").text(json.weight+"% VOTE = ");
            $("#downvalue").text("-"+json.weightvalue+" SBD");
            $("#downvote_weight").val(json.weight*-100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};
