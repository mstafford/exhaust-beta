from activities.models import Activity, activityType
from maptest.models import CragBorder
from django.contrib.auth.models import User
import pprint

class ClimbingSession:
    outdoor = 1
    style = 'Sport'
    intention = 'Casual'
    routes = []
    problems = []
    location = {}

    def __init__(self, via_xhstDB, style, intention, outdoor=0, location=""):
        if via_xhstDB:
            self.outdoor = via_xhstDB.outdoor
            self.location['name']=via_xhstDB.name
            self.style = style
            self.intention = intention
        else:
            self.outdoor = outdoor
            self.style = style
            self.intention = intention
            self.location['name']=location

    def add_pitch(self, via_xhstDB, quality="No Takes",name='',height=15,draws=0,difficulty='5.8',notes='',):
        if self.outdoor and via_xhstDB:
            details = {
                'name':name,
                'quality':quality,
                'height':height,
                'draws':draws,
                'difficulty':difficulty,
                'notes':notes
            }
            crag = CragBorder.objects.get(pk=via_xhstDB)
            print(crag.name)
        elif not self.outdoor:
            details = {
                'quality':quality,
                'height':height,
                'draws':draws,
                'difficulty':difficulty,
                'notes':notes
            }
        self.routes.append(details)

    def add_problem(self, via_xhstDB, quality="Clean Send",name="",height=3,difficulty='V1',notes='',attempts=1):
        if self.outdoor and via_xhstDB:
            details = {
                    'name':name,
                    'quality':quality,
                    'attempts':attempts,
                    'height':height,
                    'difficulty':difficulty,
                    'notes':notes
                }
        elif not self.outdoor:
            details = {
                'quality':quality,
                'attempts':attempts,
                'height':height,
                'difficulty':difficulty,
                'notes':notes
            }
        self.problems.append(details)

    def summary(self):
        json_data = {}
        json_data['climbing']={
            'outdoor':self.outdoor,
            'style':self.style,
            'intention': self.intention,
        }
        json_data['routes'] = self.routes
        json_data['problems'] = self.problems
        json_data['location']=self.location
        return(json_data)
