from activities.models import Activity, activityType, Stat

from beem.account import Account
from beem.steem import Steem
from datetime import datetime
from beem.comment import Comment

import re

def newsletter():
    now = datetime.now()
    post = ""
    exhaust = Account('exhaust')

    ### Construct post header
    title = "## <center>State of Exhaustion</center>\n"
    subtitle1 = "### <center>*" + now.strftime('%B %d, %Y') + "*</center>\n"
    sectionbreak = "\n***\n\n"
    body = ""
    footer = ""

    ### Construct Activity Highlights
    featuredacts = ""
    acts = Activity.objects.filter(posted=True).order_by('postdate').reverse()[:3]
    featurephotos=[]

    ## Make django model for featured activities. Delete after using...?
    featurecounter = 0
    featuredacts += "| | | |\n"
    featuredacts += "|-|-|-|\n"
    for feature in acts:
        comment = Comment(feature.permlink)
        firstimage = re.findall('!\[.{0,50}\]\(.{0,150}\)',comment.body)[0]
        featurephotos.append(firstimage)
        if featurecounter %2 == 0:
            featuredacts += "|"+firstimage+"|"
            featuredacts += "[" + re.sub("\|","",feature.title) + "](https://xhaust.me/activities/" + str(feature.pk)+"): a " + str(feature.type.type) + " activity by @" + feature.user.username +"|"
            if feature.type.type == "running":
                featuredacts += "<center>*Distance: " + str(round(feature.distance/1000,2)) + "km, Duration: " + str(int(feature.duration/60)) + " minutes, Pace: " + feature.pace + " min/km*</center>"
            if feature.type.type == "hiking":
                featuredacts += "<center>*Distance: " + str(round(feature.distance/1000,2)) + "km, Duration: " + str(int(feature.duration/60)) + " minutes*</center>"
            if feature.type.type == "cycling":
                speed = feature.distance / feature.duration  ## speed in m/s
                speed = str(round(speed*3.6,2)) ## speed in km/hr (3600s/hr, 1000m/km)
                featuredacts += "<center>*Distance: " + str(round(feature.distance/1000,2)) + "km, Duration: " + str(int(feature.duration/60)) + " minutes, Speed: " + speed + "km/hr*</center>"
            if feature.type.type == "yoga":
                featuredacts += "<center>*Duration: " + str(int(feature.duration/60)) + " minutes*</center>"
            featuredacts += "|\n"
        else:
            featuredacts += "|"
            if feature.type.type == "running":
                featuredacts += "<center>*Distance: " + str(round(feature.distance/1000,2)) + " km, Duration: " + str(int(feature.duration/60)) + " minutes, Pace: " + feature.pace + "min/km*</center>"
            if feature.type.type == "hiking":
                featuredacts += "<center>*Distance: " + str(round(feature.distance/1000,2)) + "km, Duration: " + str(int(feature.duration/60)) + " minutes*</center>"
            if feature.type.type == "cycling":
                speed = feature.distance / feature.duration  ## speed in m/s
                speed = str(round(speed*3.6,2)) ## speed in km/hr (3600s/hr, 1000m/km)
                featuredacts += "<center>*Distance: " + str(round(feature.distance/1000,2)) + "km, Duration: " + str(int(feature.duration/60)) + " minutes, Speed: " + speed + " km/hr*</center>"
            if feature.type.type == "yoga":
                featuredacts += "<center>*Duration: " + str(int(feature.duration/60)) + " minutes*</center>"
            featuredacts += "|[" + re.sub("\|","",feature.title) + "](https://xhaust.me/activities/" + str(feature.pk)+"): a " + str(feature.type.type) + " activity by @" + feature.user.username
            featuredacts += "|"+firstimage+"|\n"
        featurecounter += 1


    ### Construct Resteem Highlights
    blog = exhaust.get_blog()
    resteems = ''
    counter = 0
    resteems += "| | | |\n"
    resteems += "|-|-|-|\n"
    firstimage = ""
    for entry in blog:
        if entry.author != 'exhaust':
            comment = Comment(entry.author+"/"+entry.permlink)
            if 'video' in comment.json_metadata:
#                firstimage = re.findall("'https://ipfs.io/ipfs/.{1,256}'",comment.body)
#                firstimage = [re.sub("'",'',firstimage[0]),'null']
                try:
                    firstimage = ['https://ipfs.io/ipfs/'+entry.json_metadata['video']['ipfs']['snaphash']]
                except:
                    firstimage = [entry.json_metadata['video']['thumbnailUrl']]
            else:
                firstimage = re.findall('!\[.{0,50}]\(.{5,150}\)',comment.body)
#            print("=============================")
#            print(len(firstimage))
            ## Add a line to remove "|" from Titles!!!!
            if counter %2 == 0:
                try:
                    resteems += "|" + firstimage[0] + "|"
                except:
                    resteems += "| |"
                resteems += "[" + re.sub("\|","",entry.title) + "](https://xhaust.me/" + entry.parent_permlink + "/@"+entry.author+"/"+entry.permlink + ")|*By @" + entry.author + "*|\n"
            else:
                resteems += "|*By @" + entry.author + "*|[" + re.sub("\|","",entry.title) + "](https://xhaust.me/" + entry.parent_permlink + "/@"+entry.author+"/"+entry.permlink + ")"
                try:
                    resteems += "|" + firstimage[0] + "|\n"
                except:
                    resteems += "| |\n"
            counter += 1
            if counter >= 3:
                break

    ### Construct Leaderboard Section
#    runleaderWTD = LeaderBoard.objects.all().order_by('rundistanceWTD').reverse().exclude(rundistanceWTD=0)[:5]
    runleaderWTD = Stat.objects.all().filter(atype=activityType.objects.get(type="running")).order_by('WTDdist').reverse().exclude(WTDdist=0)[:5]
    runleaderMTD = Stat.objects.all().filter(atype=activityType.objects.get(type="running")).order_by('MTDdist').reverse().exclude(MTDdist=0)[:5]
    runleaderYTD = Stat.objects.all().filter(atype=activityType.objects.get(type="running")).order_by('YTDdist').reverse().exclude(YTDdist=0)[:5]
    bikeleaderWTD = Stat.objects.all().filter(atype=activityType.objects.get(type="cycling")).order_by('WTDdist').reverse().exclude(WTDdist=0)[:5]
    bikeleaderMTD = Stat.objects.all().filter(atype=activityType.objects.get(type="cycling")).order_by('MTDdist').reverse().exclude(MTDdist=0)[:5]
    bikeleaderYTD = Stat.objects.all().filter(atype=activityType.objects.get(type="cycling")).order_by('YTDdist').reverse().exclude(YTDdist=0)[:5]



#    runleaderMTD = LeaderBoard.objects.all().order_by('rundistanceMTD').reverse().exclude(rundistanceMTD=0)[:5]
#    runleaderYTD = LeaderBoard.objects.all().order_by('rundistanceYTD').reverse().exclude(rundistanceYTD=0)[:5]
#    bikeleaderWTD = LeaderBoard.objects.all().order_by('bikedistanceWTD').reverse().exclude(bikedistanceWTD=0)[:5]
#    bikeleaderMTD = LeaderBoard.objects.all().order_by('bikedistanceMTD').reverse().exclude(bikedistanceMTD=0)[:5]
#    bikeleaderYTD = LeaderBoard.objects.all().order_by('bikedistanceYTD').reverse().exclude(bikedistanceYTD=0)[:5]



#    hikeleaderWTD = LeaderBoard.objects.all().order_by('hikedistanceWTD').reverse().exclude(hikedistanceWTD=0)[:5]
#    hikeleaderMTD = LeaderBoard.objects.all().order_by('hikedistanceMTD').reverse().exclude(hikedistanceMTD=0)[:5]
#    hikeleaderYTD = LeaderBoard.objects.all().order_by('hikedistanceYTD').reverse().exclude(hikedistanceYTD=0)[:5]
#    yogaleaderWTD = LeaderBoard.objects.all().order_by('yogadurationWTD').reverse().exclude(yogadurationWTD=0)[:5]
#    yogaleaderMTD = LeaderBoard.objects.all().order_by('yogadurationMTD').reverse().exclude(yogadurationMTD=0)[:5]
#    yogaleaderYTD = LeaderBoard.objects.all().order_by('yogadurationYTD').reverse().exclude(yogadurationYTD=0)[:5]
    leaderboards = ""
    monthly = ''
    ytd = ''
    weekly = ''
    runners = '\n## <center>Running Leaders</center>\n\n'
    bikers = '\n## <center>Cycling Leaders</center>\n\n'
#    hikers = '## <center>Hiking Leaders</center>\n'
#    yoga = '## <center>Yoga Leaders</center>\n'
    runners += "||This Week|This Month|This Year|\n"
    runners += "|-|-|-|-|\n"
    bikers += "||This Week|This Month|This Year|\n"
    bikers += "|-|-|-|-|\n"
#    hikers += "||This Week|This Month|This Year|\n"
#    hikers += "|-|-|-|-|\n"
#    yoga += "||This Week|This Month|This Year|\n"
#    yoga += "|-|-|-|-|\n"
    for i in range(0,4):
        runners += "|"+str(i+1)+"|"
        bikers += "|"+str(i+1)+"|"
#        hikers += "|"+str(i+1)+"|"
#        yoga += "|"+str(i+1)+"|"
        try:
            runners += "["+runleaderWTD[i].user.username+"](https://xhaust.me/@" +runleaderWTD[i].user.username + "/) - " + str(round(runleaderWTD[i].WTDdist/1000,2)) + "km"
        except IndexError:
            runners += "N/A"
        runners += "|"
        try:
            runners += "["+runleaderMTD[i].user.username+"](https://xhaust.me/@" +runleaderMTD[i].user.username + "/) -  " + str(round(runleaderMTD[i].MTDdist/1000,2)) + "km"
        except IndexError:
            runners += "N/A"
        runners += "|"
        try:
            runners += "["+runleaderYTD[i].user.username+"](https://xhaust.me/@" +runleaderYTD[i].user.username + "/) - " + str(round(runleaderYTD[i].YTDdist/1000,2)) + "km"
        except IndexError:
            runners += "N/A"
        runners += "|\n"
        try:
            bikers += "["+bikeleaderWTD[i].user.username+"](https://xhaust.me/@" +bikeleaderWTD[i].user.username + "/) - " + str(round(bikeleaderWTD[i].WTDdist/1000,2)) + "km"
        except IndexError:
            bikers += "N/A"
        bikers += "|"
        try:
            bikers += "["+bikeleaderMTD[i].user.username+"](https://xhaust.me/@" +bikeleaderMTD[i].user.username + "/) - " + str(round(bikeleaderMTD[i].MTDdist/1000,2)) + "km"
        except IndexError:
            bikers += "N/A"
        bikers += "|"
        try:
            bikers += "["+bikeleaderYTD[i].user.username+"](https://xhaust.me/@" +bikeleaderYTD[i].user.username + "/) - " + str(round(bikeleaderYTD[i].YTDdist/1000,2)) + "km"
        except IndexError:
            bikers += "N/A"
        bikers += "|\n"
#        try:
#            hikers += hikeleaderWTD[i].user.username + " - " + str(round(hikeleaderWTD[i].hikedistanceWTD,2)) + "km"
#        except IndexError:
#            hikers += "N/A"
#        hikers += "|"
#        try:
#            hikers += hikeleaderMTD[i].user.username + " - " + str(round(hikeleaderMTD[i].hikedistanceMTD,2)) + "km"
#        except IndexError:
#            hikers += "N/A"
#        hikers += "|"
#        try:
#            hikers += hikeleaderYTD[i].user.username + " - " + str(round(hikeleaderYTD[i].hikedistanceYTD,2)) + "km"
#        except IndexError:
#            hikers += "N/A"
#        hikers += "|\n"
#        try:
#            yoga += yogaleaderWTD[i].user.username + " - " + str(yogaleaderWTD[i].yogadurationWTD) + " minutes"
#        except IndexError:
#            yoga += "N/A"
#        yoga += "|"
#        try:
#            yoga += yogaleaderMTD[i].user.username + " - " + str(yogaleaderMTD[i].yogadurationMTD) + " minutes"
#        except IndexError:
#            yoga += "N/A"
#        yoga += "|"
#        try:
#            yoga += yogaleaderYTD[i].user.username + " - " + str(yogaleaderYTD[i].yogadurationYTD) + " minutes"
#        except IndexError:
#            yoga += "N/A"
#        yoga += "|\n"

    leaderboards += runners
    leaderboards += bikers
#    leaderboards += hikers
#    leaderboards += yoga
#    leaderboards += "Starting March 1st, 2019 -- @exhaust will be awarding 1 @steembasicincome share to the winner of each weekly leaderboard!\n"
    leaderboards += "\nSteemBasicIncome share prizes are temporarily on hold while we fix some stuff :) Coming back soon, we hope.\n"
    ### Compile Footer


    footer += "The delegation system for @exhaust users is now live! There is no pressure to delegate, but if you choose to, you should start receiving payments 7 days after the delegation is made! @exhaust strives to pay back 100% of the curation rewards that delegated steem power earns!\n"
    footer += "Here are some handy quick-links for delegating to @exhaust via SteemConnect! (Links subject to change w/ upcoming SteemConnect v3...\n"
    footer += "## Delegate : [25 SP](https://steemconnect.com/sign/delegate-vesting-shares?delegatee=exhaust&vesting_shares=25%20SP) || [50 SP](https://steemconnect.com/sign/delegate-vesting-shares?delegatee=exhaust&vesting_shares=50%20SP) || [100 SP](https://steemconnect.com/sign/delegate-vesting-shares?delegatee=exhaust&vesting_shares=100%20SP) || [250 SP](https://steemconnect.com/sign/delegate-vesting-shares?delegatee=exhaust&vesting_shares=250%20SP) || [500 SP](https://steemconnect.com/sign/delegate-vesting-shares?delegatee=exhaust&vesting_shares=500%20SP) || [1,000 SP](https://steemconnect.com/sign/delegate-vesting-shares?delegatee=exhaust&vesting_shares=1000%20SP) || [10,000 SP](https://steemconnect.com/sign/delegate-vesting-shares?delegatee=exhaust&vesting_shares=10000%20SP)\n"

    ### Putting it all together
    post += title
    post += subtitle1
    post += sectionbreak
    post += "<center>![banner](https://xhaust.me/media/xhstbanner.jpg)</center>\n"
    post += "\n<center>A friendly place to try hard! Come get EXHAUSTED!</center>\n"
    post += sectionbreak
    post += "# <center>Featured Activities from Yesterday:</center>\n"
    post += featuredacts
    post += sectionbreak
    post += "# <center>Cool Stories We Liked:</center>\n"
    post += resteems
    post += sectionbreak
    post += "# <center>EXHAUST Leaderboards:</center>\n"
    post += "\n<center>\n"
    post += leaderboards
    post += "\n</center>\n"
    post += sectionbreak
    post += footer
    return(post)

def simple_activity(act):
    if act.type.type == 'yoga':  
        body_text = ("I just finished a " +
                    act.type.type +
                    " activity that lasted about " +
                    str(int(act.duration/60/60)) +
                    "hh:" + str(int(act.duration%3600/60)) +
                    "mm:" + str(act.duration%60) +
                    "ss !\n<center>![image](https://xhaust.me" + act.photo.url + ")</center>\n\n" + act.comments)
    elif act.type.type == 'strength':
        body_text = ("I just finished a " +
                    act.type.type +
                    " training activity that lasted about " +
                    str(int(act.duration/60/60)) +
                    "hh:" + str(int(act.duration%3600/60)) +
                    "mm:" + str(act.duration%60) +
                    "ss !\n<center>![image](https://xhaust.me" + act.photo.url + ")</center>\n\n" + act.comments)
    else:
        body_text = ("I just finished a " +
                    str(act.distance/1000) +
                    "km " +
                    act.type.type +
                    " that lasted about " +
                    str(int(act.duration/60/60)) +
                    "hh:" + str(int(act.duration%3600/60)) +
                    "mm:" + str(act.duration%60) +
                    "ss !\n<center>![image](https://xhaust.me" + act.photo.url + ")</center>\n\n" + act.comments)
    if act.photo2:
        body_text += "\n<center>![image](https://xhaust.me" + act.photo2.url + ")</center>\n"
    if act.photo3:
        body_text += "\n<center>![image](https://xhaust.me" + act.photo3.url + ")</center>\n"
    body_text += "\nCheck out some detailed info at [my EXHAUST page](https://xhaust.me/activities/"+str(act.pk)+"/)"+"\nJoin me in testing out [EXHAUST](https://xhaust.me)!"

    return(body_text)

def longform_init(act):
    pass
