from .models import Activity, activityType, Stat

from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.db.models import Count

class XhstStats:

    #Objects for all activity types
    alltypes = activityType.objects.all()
#    running = activityType.objects.get(type='running')
#    cycling = activityType.objects.get(type='cycling')
#    hiking = activityType.objects.get(type='hiking')
#    climbing = activityType.objects.get(type='climbing')
#    yoga = activityType.objects.get(type='yoga')
#    strength = activityType.objects.get(type='strength')
    #Placeholder for user object.
    user = User()

    #Objects for timeranges
    now = datetime.now()
    WTDstart = now - timedelta(days=now.isoweekday())
    MTDstart = datetime(now.year,now.month,1)
    YTDstart = datetime(now.year,1,1)
    ALLstart = datetime(2000,1,1)
    ROLL7start = now - timedelta(days=7)
    ROLL30start = now - timedelta(days=30)
    ROLL90start = now - timedelta(days=60)
    ROLL365start = now - timedelta(days=90)

    def __init__(self, xhstuser):
        self.user = User.objects.get(username=xhstuser)

    def dist_time_tally(self,acts):
        distsum = 0
        disttime = 0
        for idx, act in enumerate(acts):
#            if idx == 0:
#                distsum = 0
#                disttime = 0
#            else:
            distsum += act.distance
            disttime += act.duration
        return(distsum,disttime)


    def update_user_stats(self, user):
        for sport in self.alltypes:
            useracts = Activity.objects.filter(user=user, type=sport, posted=True)
#            print(useracts)
            stats = Stat.objects.get_or_create(user=user, atype=sport)[0]
            distsum = 0
            disttime = 0
            # Grab stats for rolling 365 days
            actset = useracts.filter(postdate__gte=self.ROLL365start)
            stats.ROLL365count = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.ROLL365dist = distsum
            stats.ROLL365time = disttime
            # Grab stats for rolling 90 days
            actset = useracts.filter(postdate__gte=self.ROLL90start)
            stats.ROLL90count = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.ROLL90dist = distsum
            stats.ROLL90time = disttime
            # Grab stats for rolling 30 days
            actset = useracts.filter(postdate__gte=self.ROLL30start)
            stats.ROLL30count = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.ROLL30dist = distsum
            stats.ROLL30time = disttime
            # Grab stats for rolling 7 days
            actset = useracts.filter(postdate__gte=self.ROLL7start)
            stats.ROLL7count = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.ROLL7dist = distsum
            stats.ROLL7time = disttime
            # Grab stats for ALL TIME
            actset = useracts.filter(postdate__gte=self.ALLstart)
            stats.ALLcount = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.ALLdist = distsum
            stats.ALLtime = disttime
            # Grab stats for YTD
            actset = useracts.filter(postdate__gte=self.YTDstart)
            stats.YTDcount = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.YTDdist = distsum
            stats.YTDuime = disttime
            # Grab stats for MTD
            actset = useracts.filter(postdate__gte=self.MTDstart)
            stats.MTDcount = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.MTDdist = distsum
            stats.MTDtime = disttime
            # Grab stats for WTD
            actset = useracts.filter(postdate__gte=self.WTDstart)
            stats.WTDcount = len(actset)
            distsum, disttime = self.dist_time_tally(actset)
            stats.WTDdist = distsum
            stats.WTDtime = disttime
            stats.save()
#        print(self.user.username)
        for act in useracts:
            print(act.title)

    def update_all_users(self):
        users = User.objects.annotate(num_act=Count('activities')).filter(num_act__gt=0)
#        users = User.objects.all()
        for user in users:
            print(user.username)
            self.user = user
            self.update_user_stats(self.user)

    def frontpage_leaders(self):
        ## Return WTD leaderboards for front page
        stats = Stat.objects.all()
        running = self.alltypes[0]
        cycling = self.alltypes[1]
        runners = stats.filter(atype=running)
        cyclists = stats.filter(atype=cycling)
        runcountleaders = runners.order_by('WTDcount').reverse()[:3]
        rundistleaders = runners.order_by('WTDdist').reverse()[:3]
        runtimeleaders = runners.order_by('WTDtime').reverse()[:3]

        bikecountleaders = cyclists.order_by('WTDcount').reverse()[:3]
        bikedistleaders = cyclists.order_by('WTDdist').reverse()[:3]
        biketimeleaders = cyclists.order_by('WTDtime').reverse()[:3]

        return(runcountleaders,rundistleaders,runtimeleaders, bikecountleaders,bikedistleaders,biketimeleaders)
