from django.db import models
from django.contrib.auth.models import User

class activityType(models.Model):
    type = models.CharField(max_length=32)

    def __str__(self):
        return self.type

class Newsletter(models.Model):
    permlink = models.CharField(max_length=128)
    title = models.CharField(max_length=128)
    img_url = models.CharField(max_length=128)
    uploaded_at = models.DateTimeField(auto_now_add=True)

class UserProfile(models.Model):
    url = models.URLField()
    user = models.OneToOneField(User, unique = True, on_delete=models.CASCADE)
    avatar = models.URLField()

class Activity(models.Model):
    ascent = models.FloatField(default=0.0)
    json_data = models.TextField(blank=True)
    comments = models.TextField(max_length=4096, blank=True)
    coach = models.CharField(max_length=120, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    descent = models.FloatField(default=0.0)
    distance = models.IntegerField(default=0, blank=True)  ##length in metres
    duration = models.IntegerField(default=0, blank=True) ##time in seconds
    equipment = models.CharField(max_length=256, blank=True)
    gpsfile = models.FileField(upload_to='documents/locData/', blank=True)
    effort_ratio = models.FloatField(default=1.0)
    muscles = models.CharField(max_length=128, blank=True)
    pace = models.CharField(max_length=10, blank=True)
    permlink = models.CharField(max_length=128, blank=True)
    photo = models.ImageField(upload_to='documents/images/', blank=True)
    photo2 = models.ImageField(upload_to='documents/images/', blank=True)
    photo3 = models.ImageField(upload_to='documents/images/', blank=True)
    postdate = models.DateTimeField(auto_now_add=True)
    posted = models.BooleanField(default=False)
    rewards = models.FloatField(default=0.0)
#    showmap = models.IntegerField(choices=MAP_PRIVACY_CHOICES,default=1)
    speed = models.CharField(max_length=10, blank=True)
    steps = models.IntegerField(default=0)
    start = models.DateTimeField(auto_now_add=True)
    strength_data = models.TextField(max_length=4096, blank=True)
    style = models.CharField(max_length=120,blank=True)
    tags = models.TextField(max_length=128, blank=True)
    title = models.CharField(max_length=120, blank=False, default="New Activity Title")
    type = models.ForeignKey(activityType, on_delete=models.CASCADE)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name="activities", on_delete=models.CASCADE)
    verb = models.CharField(max_length=10, blank=True)
    video = models.CharField(max_length=256, blank=True)
    def __str__(self):
        return (str(self.user.username) + " - " + str(self.type) + str(self.pk))

class Stat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    atype = models.ForeignKey(activityType, on_delete=models.CASCADE)
    WTDdist = models.IntegerField(default=0, blank=True)
    WTDtime = models.IntegerField(default=0, blank=True)
    WTDcount = models.IntegerField(default=0, blank=True)
    MTDdist = models.IntegerField(default=0, blank=True)
    MTDtime = models.IntegerField(default=0, blank=True)
    MTDcount = models.IntegerField(default=0, blank=True)
    YTDdist = models.IntegerField(default=0, blank=True)
    YTDtime = models.IntegerField(default=0, blank=True)
    YTDcount = models.IntegerField(default=0, blank=True)
    ALLdist = models.IntegerField(default=0, blank=True)
    ALLtime = models.IntegerField(default=0, blank=True)
    ALLcount = models.IntegerField(default=0, blank=True)
    ROLL7dist = models.IntegerField(default=0, blank=True)
    ROLL7time = models.IntegerField(default=0, blank=True)
    ROLL7count = models.IntegerField(default=0, blank=True)
    ROLL30dist = models.IntegerField(default=0, blank=True)
    ROLL30time = models.IntegerField(default=0, blank=True)
    ROLL30count = models.IntegerField(default=0, blank=True)
    ROLL90dist = models.IntegerField(default=0, blank=True)
    ROLL90time = models.IntegerField(default=0, blank=True)
    ROLL90count = models.IntegerField(default=0, blank=True)
    ROLL365dist = models.IntegerField(default=0, blank=True)
    ROLL365time = models.IntegerField(default=0, blank=True)
    ROLL365count = models.IntegerField(default=0, blank=True)
    def __str__(self):
        return (str(self.user.username) + " - " + str(self.atype.type))






