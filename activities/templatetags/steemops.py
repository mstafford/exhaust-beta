from activities.models import UserProfile
from beem.account import Account
from django import template
from django.contrib.auth.models import User

register = template.Library()

@register.filter
def grab_avatar(username):
    user, created = User.objects.get_or_create(username=username)
    profile,created=UserProfile.objects.get_or_create(user=user)
    if profile.avatar == '':
        try:
            account = Account(username)
            profile.avatar = account.json_metadata['profile']['profile_image']
            profile.save()
        except:
            print(str(username) + ' avatar not found')
            profile.avatar = "/media/avatar_default.png"
            profile.save()
    elif profile.avatar == 'avatar_default.png':
        profile.avatar = "/media/avatar_default.png"
        profile.save()
    return (profile.avatar)
