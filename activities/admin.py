from django.contrib import admin

from .models import Activity, activityType, Newsletter, Stat, UserProfile

admin.site.register(Activity)
admin.site.register(activityType)
admin.site.register(Stat)
admin.site.register(UserProfile)
admin.site.register(Newsletter)
