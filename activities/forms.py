from activities.models import Activity, activityType
from django import forms
from django.forms import ModelForm
from markdownx.fields import MarkdownxFormField
#from markdownx.models import MarkdownxField

import floppyforms as floppy
import floppyforms.__future__ as floppymodel

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ('title', 'type', 'comments', 'distance', 'duration', 'gpsfile', 'photo', 'photo2', 'photo3' )
        help_texts = {
            'distance': 'Activity Distance in Metres',
            'duration': 'Activity Duration in Seconds',
            'gpsfile': 'Distance and Duration data automatically overwritten if GPS File uploaded',
        }

class Slider(floppy.RangeInput):  ##SLIDER WIDGET
    min = 5
    max = 100
    step = 5
    template_name = 'activities/slider.html'

    class Media:
        js = (
            'activities/assets/js/jquery-3.4.1.min.js',
            'activities/assets/js/jquery-ui.min.js',
        )
        css = {
            'all': (
                'activities/assets/css/jquery-ui.css',
            )
        }

class UpvoteForm(floppy.Form):
    upvote = floppy.IntegerField(widget=Slider)
    min = 5
    max = 100
    step = 5

    def clean_num(self):
        upvote = self.cleaned_data['upvote']
        if not 5 <= upvote <= 100:
            raise forms.ValidationError("Enter a value between 5 and 20")

        if not upvote % 1 == 0:
            raise forms.ValidationError("Enter a multiple of 5")
        return upvote

class DownvoteForm(floppy.Form):
    downvote = floppy.IntegerField(widget=Slider)
    min = 5
    max = 100
    step = 5

    def clean_num(self):
        downvote = self.cleaned_data['downvote']
        if not 5 <= downvote <= 100:
            raise forms.ValidationError("Enter a value between 5 and 20")

        if not downvote % 1 == 0:
            raise forms.ValidationError("Enter a multiple of 5")
        return downvote

class CommentForm(forms.Form):
    commentbody = MarkdownxFormField()


class Textarea(floppy.Textarea):
    template_name = 'floppyforms/textarea.html'
    rows = 5
    cols = 30

class NoteForm(floppy.Form):
    Activity_Notes = floppy.CharField(widget=Textarea)

class PostForm(forms.Form):
    post_body = MarkdownxFormField()

class TitleForm(forms.Form):
    title = forms.CharField(min_length=3, max_length=100, required = True, label="Title", initial="My Exhausting Blog Entry", help_text="100 Characters Max!")

class ActivityTypeForm(ModelForm):
    class Meta:
        model = activityType
        fields = ['type']

class ActivityGPSManual(ModelForm):
    running = activityType.objects.get(type="running")
    cycling = activityType.objects.get(type="cycling")
    hiking = activityType.objects.get(type="hiking")
    CHOICES = (
        (1, 'Running'),
        (2, 'Cycling'),
        (3, 'Hiking'),
    )
    category = forms.ChoiceField(choices=CHOICES)
    class Meta:
        model = Activity
        fields = ['title','distance','duration', 'photo', 'photo2','photo3']

class ActivityGPSFile(ModelForm):
    running = activityType.objects.get(type="running")
    cycling = activityType.objects.get(type="cycling")
    hiking = activityType.objects.get(type="hiking")
    CHOICES = (
        (1, 'Running'),
        (2, 'Cycling'),
        (3, 'Hiking'),
    )
    category = forms.ChoiceField(choices=CHOICES)
    class Meta:
        model = Activity
        fields = ['title','gpsfile', 'photo', 'photo2','photo3']

class TagForm(forms.Form):
    tags = forms.CharField(max_length=256,required=True, help_text="Must start with a lowercase letter.")
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tags'].widget.attrs.update({'data-role': 'tagsinput'})
