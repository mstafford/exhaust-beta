## Imports from Django DB's
from activities.models import Activity, activityType
from django.contrib.auth.models import User

## Import other modules
from beem.amount import Amount
from beem.comment import Comment
from datetime import datetime
import json
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def check_date():
    ## Declare timeframes
    now = datetime.utcnow()
    sevenday = now.timestamp()-60*60*24*7
    sevendayend = now.timestamp()-60*60*24*7*2
    thirtyday = now.timestamp()-60*60*24*30
    thirtydayend = now.timestamp()-60*60*24*30*2
    ninetyday = now.timestamp()-60*60*24*90
    ninetydayend = now.timestamp()-60*60*24*90*2
    return(now,sevenday,sevendayend,thirtyday,thirtydayend,ninetyday,ninetydayend)

class ReportCharts:
    username = ''
    sport = ''
    timeframe = ''

    def __init__(self,username, sport, timeframe):
        self.username = username
        self.sport = sport
        self.timeframe = timeframe

    now, sevenday,sevendayend, thirtyday,thirtydayend, ninetyday,ninetydayend = check_date() #Check the time, yo!
    now = datetime.now().timestamp()

    def get_frequency_chart(self):
        dates = []
        activities = []
        user = User.objects.get(username=self.username)
        atype = activityType.objects.get(type=self.sport)
        if self.timeframe == "sevenday":
            days = 6
            for day in range(0,days):
                acts = Activity.objects.filter(user=user,type=atype,uploaded_at__gte=datetime.fromtimestamp(self.sevenday-(days+day)*60*60*24),uploaded_at__lte=datetime.fromtimestamp(self.sevenday-day*60*60*24)).order_by('uploaded_at')
                counter = 0
#                for act in acts:
#                    counter += 1
                activities.append(len(acts))
                dates.append(-days+day)
        elif self.timeframe == "thirtyday":
            days = 29
            for day in range(0,days):
                acts = Activity.objects.filter(user=user,type=atype,uploaded_at__gte=datetime.fromtimestamp(self.thirtyday-(days+day)*60*60*24),uploaded_at__lte=datetime.fromtimestamp(self.thirtyday-day*60*60*24)).order_by('uploaded_at')
                counter = 0
#                for act in acts:
#                    counter += 1
                activities.append(len(acts))
                dates.append(-days+day)
        elif self.timeframe == "ninetyday":
            days = 89
            for day in range(0,days):
                acts = Activity.objects.filter(user=user,type=atype,uploaded_at__gte=datetime.fromtimestamp(self.ninetydayend+day*60*60*24),uploaded_at__lte=datetime.fromtimestamp(self.ninetyday+day*60*60*24)).order_by('uploaded_at')
                counter = 0
#                for act in acts:
#                    counter += 1
                print(len(acts),day)
                activities.append(len(acts))
                dates.append(-days+day)


        fig, ax = plt.subplots()
        ax.plot(dates, activities)
        ax.set(xlabel='Days from Today',ylabel='# of Activities',title='Rolling Activity Count')
        ax.grid()
        fig.savefig(f"media/reports/{self.username}-report_count-{int(self.now)}.png")
        plt.show()

def get_distance_charts(username):
    now, sevenday,sevendayend, thirtyday,thirtydayend, ninetyday,ninetydayend = check_date() #Check the time, yo!
    dist_types = activityType.objects.filter(type__in=['running','cycling','hiking'])
#    running = activityType.objects.get(type="running") #Django DB set filter. This is probably dumb
    chart_json = {}
    for dist_type in dist_types:
        chart_json[dist_type.type]={}
        user = User.objects.get(username=username) #Lookup the user that's requesting this
        acts = Activity.objects.filter(user=user,type=dist_type) #Get all of the users activities of this type
#        if len(acts) <= 0:
#            chart_json[dist_type.type]='null'
#            continue
        timeframes = {
            'rollingseven': {'start':sevenday,'end':sevendayend,'iterations':7,'title':'Rolling 7 Day Total Distance (km)'}, #variables for timeframe
            'rollingthirty': {'start':thirtyday,'end':thirtydayend,'iterations':30,'title':'Rolling 30 Day Total Distance (km)'}, #variables for timeframe
            'rollingninety': {'start':ninetyday,'end':ninetydayend,'iterations':90,'title':'Rolling 90 Day Total Distance (km)'}, #variables for timeframe
        }
        ## For each timeframe, iterate through each day up until today
        ## While checking X days behind, and summing the distance/time/stat
        for timeframe in timeframes:
            cumulative_dist = 0
            chart_json[dist_type.type][timeframe]=[]
            for point in range(0,timeframes[timeframe]['iterations']):
                cumulative_dist = 0
                time_range_start = datetime.utcfromtimestamp(timeframes[timeframe]['start']+point*60*60*24)
                time_range_end = datetime.utcfromtimestamp(timeframes[timeframe]['end']+point*60*60*24)
                sample = acts.filter(uploaded_at__gte=time_range_end,uploaded_at__lte=time_range_start)
                for act in sample:
                   cumulative_dist += act.distance
                chart_json[dist_type.type][timeframe].append({
                    'date': 'new Date('+str(time_range_start.year)+','+str(time_range_start.month)+','+str(time_range_start.day)+')',
                    'value':cumulative_dist/1000
                })

        cumulative_dist=0
    return(chart_json['running'], chart_json['cycling'], chart_json['hiking'])

def generate_report(username, sport, timeframe):
    now, sevenday,sevendayend, thirtyday,thirtydayend, ninetyday,ninetydayend = check_date() #Check the time, yo!
    atype = activityType.objects.get(type=sport)
    user = User.objects.get(username=username)
    numdays = 0
    allacts = Activity.objects.filter(user=user, type=atype, posted=True)
    reportcharts = ReportCharts(username, sport, timeframe)
    if timeframe == "sevenday":
        acts = allacts.filter(uploaded_at__gte=datetime.fromtimestamp(sevenday)).order_by('uploaded_at').reverse()
        otheracts = allacts.filter(uploaded_at__lt=datetime.fromtimestamp(sevenday))
        report_template = f"### <center>@{user.username}'s seven-day {atype.type} Report:</center>\n"
        numdays = 7
    elif timeframe == "thirtyday":
        acts = allacts.filter(uploaded_at__gte=datetime.fromtimestamp(thirtyday)).order_by('uploaded_at').reverse()
        otheracts = allacts.filter(uploaded_at__lt=datetime.fromtimestamp(thirtyday))
        report_template = f"### <center>@{user.username}'s thirty-day {atype.type} Report:</center>\n"
        numdays = 30
    elif timeframe == "ninetyday":
        acts = allacts.filter(uploaded_at__gte=datetime.fromtimestamp(ninetyday)).order_by('uploaded_at').reverse()
        otheracts = allacts.filter(uploaded_at__lt=datetime.fromtimestamp(ninetyday))
        report_template = f"# <center>@{user.username}'s ninety-day {atype.type} Report:</center>\n"
        numdays = 90
    report_template += f"### <center>{now.strftime('%B %d, %Y')}</center>\n\n"
    report_template += "***\n\n"
    ### Number of activities and frequency of workout
    print(len(allacts),len(acts),len(otheracts))
    report_template += "## Training Frequency:\n\n"
    actfrequency = round(numdays/len(acts),2)
    report_template += f"* I have shared {len(acts)} {atype.type} activities over the last {numdays} days! That's roughly every {actfrequency} days!\n"
    ### Compare above to historic average
    otherdays = round((now.date()-otheracts.first().uploaded_at.date()).days-numdays,2)
    otherfrequency = round(otherdays/len(otheracts),2)
    report_template += f"* Prior to this sample, I had shared {len(otheracts)} {atype.type} activities over {otherdays} days. This works out to training every {otherfrequency} days.\n\n***\n\n"
    if actfrequency <= otherfrequency:
        report_template += f"I'm training more frequently than you used to! That's dope! Neato, gang!\n\n***\n\n"
    else:
        report_template += f"I'm not training quite as much as I was before. If that's what I was hoping for -- that's great! Otherwise, maybe put some thought into why I've tapered off? Not a big deal either way though, as there's always the future!\n\n***\n\n"
    ### Highlight for Most recent, Longest Distance, Longest Duration, Fastest in sample
    latest = acts.order_by('postdate').reverse().last()
    furthest = acts.order_by('distance').reverse().last()
    longest = acts.order_by('duration').reverse().last()
    fastest = acts.order_by('pace').last()
    report_template += "\n## Hightlight Activities:\n\n"
    report_template += "|Newest|Furthest|Longest|Fastest|\n"
    report_template += "|------|--------|-------|-------|\n"
    report_template += f"|![Latest Photo](https://xhaust.me{latest.photo.url})|![Furthest Photo](https://xhaust.me{furthest.photo.url})|![Longest Photo](https://xhaust.me{longest.photo.url})|![Fastest Photo](https://xhaust.me{fastest.photo.url})|\n"
    report_template += f"|[{latest.title}](https://xhaust.me/activities/{latest.pk}/)|[{furthest.title}](https://xhaust.me/activities/{furthest.pk}/)|[{longest.title}](https://xhaust.me/activities/{longest.pk}/)|[{fastest.title}](https://xhaust.me/activities/{fastest.pk}/)|\n"
    report_template += f"|{latest.distance/1000} km|{furthest.distance/1000} km|{longest.distance/1000} km|{fastest.distance/1000} km|\n"
    report_template += f"|{int(latest.duration/60)} min|{int(furthest.duration/60)} min|{int(longest.duration/60)} min|{int(fastest.duration/60)} min|\n"
    report_template += f"|{latest.pace} min/km|{furthest.pace} min/km|{longest.pace} min/km|{fastest.pace} min/km|\n"
    ### List Sampled Activities c/w image, title (link to xhaust activity and post), distance, duration
    reportcharts.get_frequency_chart()
    report_template += f"<center>![Number of Activities](https://xhaust.me/media/reports/{username}-report_count-{int(reportcharts.now)}.png)</center>\n\n"
    report_template += f"#### <center>My {atype.type} activities checked in this report:</center>\n\n"
    report_template += "|Title|Date|Distance|Duration|Steem Post|Rewards Earned|\n"
    report_template += "|---|---|---|---|---|---|\n"
    total_distance = 0
    total_duration = 0
    total_rewards = Amount(0,asset="SBD")
    other_distance = 0
    other_duration = 0
    for act in acts:
        comment = Comment(act.permlink, use_tags_api=False)
        report_template += f"|[{act.title}](https://xhaust.me/activities/{act.pk}/)|{act.uploaded_at.strftime('%B %d')}|{round(act.distance/1000,2)}km|{int(act.duration/60)} minutes|[LINK](https://xhaust.me/{comment.category}/{act.permlink})|{comment.reward}|\n"
        total_distance += act.distance/1000
        total_duration += act.duration/60
        total_rewards += comment.reward
    for act in otheracts:
        other_distance += act.distance/1000
        other_duration += act.duration/60

    ### Total Distance (for distance activities), duration, and rewards
    report_template += f"\n#### Diggin' into the numbers a bit, this means I:\n\n"
    report_template += f"* Covered a total distance of roughly {round(total_distance,2)} km;\n"
    report_template += f"* Trained for a total duration of roughly {int(total_duration)} minutes;\n"
    report_template += f"* Earned a total of roughly {total_rewards*0.5*0.8} from authoring these posts;\n"
    report_template += f"* Contributed approximately {total_rewards*0.5*0.2} to the EXHAUST weekly payout pool;\n\n"
    ### Compare average speed / pace of this sample, to average speed / pace of *all other* activities
    report_template += f"## Distances and Durations:\n\n"
    report_template += f"Over the sampled activities:\n\n* My average distance was {round(total_distance / len(acts),2)} km; and\n* My average duration was {int(total_duration/len(acts))} minutes; for\n* an average pace of approximately {round(total_duration/total_distance,2)}min/km.\n\n"
    report_template += f"Comparing this to the averages of the rest of my {atype.type} history ({round(other_distance/len(otheracts),2)} km | {int(other_duration/len(otheracts))} minutes | {round(other_duration/other_distance,2)} min/km), it looks like:\n\n"
    if other_distance/len(otheracts) < total_distance/len(acts):
        report_template+= f"* My average {atype.type} distance has gotten longer recently; and\n"
    else:
        report_template += f"* My average {atype.type} distance has gotten shorter recently; and\n"
    if other_duration/len(otheracts) < total_duration/len(acts):
        report_template += f"* My average {atype.type} duration has lasted longer; and\n"
    else:
        report_template += f"* My average {atype.type} duration has been shorter; and\n"
    if other_duration/other_distance < total_duration/total_distance:
        report_template += f"* My average {atype.type} pace has gotten a bit slower; and\n"
    else:
        report_template += f"* My average {atype.type} pace has gotten a bit faster; and\n"

    print(report_template)
    return(report_template)
