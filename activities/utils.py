from activities.models import activityType, Activity, Newsletter
from django.contrib.auth.models import User

from beem.account import Account
from beem.comment import Comment
from bs4 import BeautifulSoup
from datetime import datetime
from keychain.models import Strava_Token
from keychain import utils as kutils
from markdown2 import Markdown
from markdownx.utils import markdownify

import json
import re
import requests

def frontPage():
    exhaust = Account('exhaust')
    entries = exhaust.get_blog(-1,50)
    blogs = []
    resteems = []
    for entry in entries:
        if not entry.is_pending():
            continue
        if entry.author != 'exhaust':
            resteems.append(entry)
        elif 'blog' in entry.json_metadata['tags']:
            blogs.append(entry)
    for entry in resteems:
        try:
            if 'video' in entry.json_metadata:
                try:
                    image_link = 'https://ipfs.io/ipfs/'+entry.json_metadata['video']['ipfs']['snaphash']
                except:
                    image_link = entry.json_metadata['video']['thumbnailUrl']
            else:
                try:
                    image_link = re.search('https.{0,256}\.jpg', entry.body)[0]
                except:
                    image_link = re.search('!\[[^\]]+]\([^\)]+\)',entry.body)
                    image_link = image_link[0]
                    image_link = re.sub('!\[.{0,64}]\(','',image_link)
                    image_link = re.sub('\)','',image_link)
        except:
            image_link = "Can't Find Header Image"
        entry.img_url=image_link
    for entry in blogs:
        try:
            image_link = re.search('https.{0,256}\.jpg', entry.body)[0]
        except:
            image_link = re.search('!\[.{0,120}\]\(.{0,120}\)',entry.body)
            image_link = image_link[0]
            image_link = re.sub('!\[.{0,64}]\(','',image_link)
            image_link = re.sub('\)','',image_link)
        entry.img_url = image_link
    return(blogs, resteems)

def update_newsletters():
    exhaust = Account('exhaust')
    entries = exhaust.get_blog(-1,50)
    newsletters = []
    newsletter = ''
    for entry in entries:
        print(entry.permlink)
        if not entry.is_pending():
            try:
                newsletter = Newsletter.objects.get(permlink = entry.permlink)
                print(newsletter)
                newsletter.delete()
                print("Deleted newsletter")
                continue
            except:
                continue
        if 'newsletter' in entry.json_metadata['tags']:
            newsletter,created = Newsletter.objects.get_or_create(permlink=entry.permlink, title=entry.title)
            newsletter.save()

def mentioner(body):
    body = body
    mentions = list(set(re.findall('@[\w.-]{1,16}', body)))
    for mention in mentions:
        mention_link = '['+mention+'](https://xhaust.me/'+mention+')'
        body = re.sub(mention,mention_link,body)
    return(body)


def get_article(request, author, permlink):
    try:
        pass
        comment = Comment("@"+author+"/"+permlink)
        markdowner = Markdown(extras=["tables"])
        body = markdowner.convert(comment.body)
        soup = BeautifulSoup(body)

        images = soup.find_all('img')
        for img in images:
            img['style']='max-width:80%;display:block;margin-left:auto;margin-right:auto'
            if "gif" not in img['src']:
                img['src']= 'https://steemitimages.com/640x480/' + img['src']
                img['height']=''

        ps = soup.find_all('p')
        for p in ps:
            p['style']=''

#        votes = comment.get_votes()
        votes = []
        downvotes = 0
        upvotes = 0
        voted=0
        for vote in votes:
            if vote.rshares < 0:
                downvotes += 1
                if vote.voter == request.user.username:
                    voted = -1
            elif vote.rshares > 0:
                upvotes += 1
                if vote.voter == request.user.username:
                    voted = 1
#        payout = comment.get_rewards()['total_payout']
        payout = comment.reward
#        replies = comment.get_replies()
        replies = 'null'
        commenters = []
#        for reply in replies:
#            commenters.append(reply.author)

#        return(comment.author, soup.prettify(), comment.title, downvotes, upvotes, payout, comment.get_replies(), comment.get_reblogged_by(),voted, commenters, comment.permlink)
        return(comment.author, soup.prettify(), comment.title, downvotes, upvotes, payout, [], [],voted, commenters, comment.permlink)
    except:
        return('null','null','null', 'null','null','null',[],[],[],[],'null')

def get_profile(user):
    try:
        account = Account(user)
        return(account.json_metadata['profile']['profile_image'])
    except:
        return("/media/avatar_default.png")

def fetch_feed(atype):
    if atype == 'all':
        acts = Activity.objects.filter(posted=True).order_by('postdate').reverse()
    else:
        act_type = activityType.objects.get(type=atype)
        acts = Activity.objects.filter(posted=True,type=act_type).order_by('postdate').reverse()
    for act in acts:
        if act.photo.name == "":
            act.photo.name = str(act.type.type)+'-default.jpg'
            act.save()
    return(acts)

def activity_steem_performance(request, permlink):
    print("Sanity check 1 - Getting performance!")
    try:
        comment = Comment(permlink)
        markdowner = Markdown()
        body = markdowner.convert(comment.body)
        soup = BeautifulSoup(body)
        images = soup.find_all('img')
        for img in images:
            img['style']='max-width:40%;padding-left:auto;padding-right:auto;margin-left:auto;margin-right:auto'

        ps = soup.find_all('p')
        for p in ps:
            p['style']='max-width:75%;margin-left:auto;margin-right:auto;padding-left:auto;padding-right:auto'

        votes = comment.get_votes()
        downvotes = 0
        upvotes = 0
        voted=0
        for vote in votes:
            if vote.rshares < 0:
                downvotes += 1
                if vote.voter == request.user.username:
                    voted = -1
            elif vote.rshares > 0:
                upvotes += 1
                if vote.voter == request.user.username:
                    voted = 1

#        payout = comment.get_rewards()['total_payout']
        payout = comment.reward
#        replies = comment.get_replies()
        commenters = []

#        for reply in replies:
#            commenters.append(reply.author)
#            reply.body = markdownify(reply.body)

#        return(comment.author, soup.prettify(), comment.title, downvotes, upvotes, payout, comment.get_replies(), comment.get_reblogged_by(),voted, commenters, comment.permlink)
        return(comment.author, soup.prettify(), comment.title, downvotes, upvotes, payout, [], [],voted, commenters, comment.permlink)
    except:
        return('null','null','null', 'null','null','null',[],[],[],[],'null')

def fetch_profile(username, visitor):
    account = Account(username)
    user = User.objects.get_or_create(username=username)[0]
    acts = Activity.objects.filter(user=user).order_by('postdate').reverse()
    following = False
    if str(visitor) in account.get_followers():
        following = True
    for act in acts[:10]:
        if act.photo.name == '':
            act.photo.name = str(act.type.type) + '-default.jpg'
            act.save()
        if not act.posted:
            continue
#        comment = Comment(act.permlink)
#        act.rewards = comment.get_rewards()['total_payout'].amount
#        act.save()
    return(account,acts,following)

def get_strava_activities(user):
    strava = Strava_Token.objects.get(user=user)
    base_url = "https://www.strava.com/api/v3/"
    athlete_url = base_url+"athlete/activities"
    headers = {"Authorization":"Bearer "+strava.access_token}
    response = requests.get(url=athlete_url, headers=headers)
    if response.status_code != 200:
        print("Old acccess_token. Renewing!")
        kutils.get_strava_token(user)
        strava = Strava_Token.objects.get(user=user)
        headers = {"Authorization":"Bearer "+strava.access_token}
        response = requests.get(url=athlete_url, headers=headers)
        if response.status_code == 401:
            print("Authorization revoked. Deleting from sync list.")
            strava.delete()
    acts = json.loads(response.content)
    atype = activityType.objects.get(type="running")
    if str(acts[0]['id']) == strava.last_act:
        print("Nothing new to see here!")
        return('null')
    else:
        if acts[0]['type']=='Run':
            atype = activityType.objects.get(type="running")
            act = Activity(
                user=user,
                distance=acts[0]['distance'],
                duration=acts[0]['moving_time'],
                type=atype,
                title=acts[0]['name'],
            )
            strava.last_act = acts[0]['id']
            strava.save()
            stream_url = base_url+"activities/"+str(strava.last_act)+"/streams"
            data={'keys':'time,distance,altitude,heartrate'}
            streams = requests.get(url=stream_url, headers=headers, data=data)
            act.json_data = json.loads(streams.content)
            act.save()
            act.date=datetime.strptime(acts[0]['start_date_local'],'%Y-%m-%dT%H:%M:%SZ')
            act.save()
            act.start=datetime.strptime(acts[0]['start_date_local'],'%Y-%m-%dT%H:%M:%SZ')
            act.save()
            act.photo="running-default.jpg"
            act.save()
            pace = (act.duration/60)/(act.distance/1000)
            act.pace = round(pace,3)
            act.save()
            act_url = base_url+"activities/"+str(strava.last_act)
            details = requests.get(url=act_url, headers=headers)
            details = json.loads(details.content)
            try:
                act.comments = details['description']
                act.save()
            except:
                pass
            return(act)
        if acts[0]['type']=='Ride':
            atype = activityType.objects.get(type="cycling")
            act = Activity(
                user=user,
                distance=acts[0]['distance'],
                duration=acts[0]['elapsed_time'],
                type=atype,
                title=acts[0]['name'],
            )
            strava.last_act = acts[0]['id']
            strava.save()
            stream_url = base_url+"activities/"+str(strava.last_act)+"/streams"
            data={'keys':'time,distance,altitude,heartrate,power'}
            streams = requests.get(url=stream_url, headers=headers, data=data)
            act.json_data = json.loads(streams.content)
            act.save()
            act.date=datetime.strptime(acts[0]['start_date_local'],'%Y-%m-%dT%H:%M:%SZ')
            act.save()
            act.start=datetime.strptime(acts[0]['start_date_local'],'%Y-%m-%dT%H:%M:%SZ')
            act.save()
            act.photo='cycling-default.jpg'
            act.save()
            pace = (act.duration/60)/(act.distance/1000)
            act.pace = round(pace,3)
            act.save()
            act_url = base_url+"activities/"+str(strava.last_act)    
            details = requests.get(url=act_url, headers=headers)
            details = json.loads(details.content)
            try:
                act.comments = details['description']
                act.save()
            except:
                pass
            return(act)
        if acts[0]['type']=='Swim':
            atype = activityType.objects.get(type="swimming")
            act = Activity(
                user=user,
                distance=acts[0]['distance'],
                duration=acts[0]['elapsed_time'],
                type=atype,
                title=acts[0]['name'],
            )
            strava.last_act = acts[0]['id']
            strava.save()
            stream_url = base_url+"activities/"+str(strava.last_act)+"/streams"
            data={'keys':'time,distance,heartrate'}
            streams = requests.get(url=stream_url, headers=headers, data=data)
            act.json_data = json.loads(streams.content)
            act.save()
            act.date=datetime.strptime(acts[0]['start_date_local'],'%Y-%m-%dT%H:%M:%SZ')
            act.save()
            act.start=datetime.strptime(acts[0]['start_date_local'],'%Y-%m-%dT%H:%M:%SZ')
            act.save()
            act.photo='swimming-default.jpg'
            act.save()
            pace = (act.duration/60)/(act.distance/1000)
            act.pace = round(pace,3)
            act.save()
            act_url = base_url+"activities/"+str(strava.last_act)    
            details = requests.get(url=act_url, headers=headers)
            details = json.loads(details.content)
            try:
                act.comments = details['description']
                act.save()
            except:
                pass
    return(acts[0])
