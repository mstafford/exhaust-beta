from activities import autoposter, stats, utils, dashboard
from activities.forms import ActivityForm, CommentForm, DownvoteForm, PostForm, TitleForm, UpvoteForm, TagForm ## Forms for Steem Ops
from activities.forms import ActivityTypeForm, NoteForm, ActivityGPSManual, ActivityGPSFile ## Forms for Uploader
from activities.models import Activity, activityType, Newsletter, Stat
from activities.permissions import IsOwnerOrReadOnly
from activities.serializers import ActivitySerializer, UserSerializer

from beem.account import Account
from beem.comment import Comment

from charts.gpxParse import importGPX, importTCX
from charts.utils import gpx_chart, tcx_chart, json_chart, gpx_map, tcx_map, leader_chart

from keychain import utils as keychain_utils
from keychain.models import Strava_Token, Polarflow_Token

from datetime import datetime
from django.contrib import messages as ms
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.views import View

from rest_framework import generics, mixins, permissions, renderers, status, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from xhstbot.forms import SettingForm
from xhstbot.models import Setting

import json

def delete_activity(request):
    #Check to make sure activity is owned by user
    user = User.objects.get(username=request.user)
    chop = Activity.objects.get(pk=request.POST['activity'])
    if user.username == chop.user.username:
        chop.delete()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def fetchreport(request):
    if request.method == 'POST':
        report = dashboard.generate_report(request.user.username,request.POST['activitytype'],request.POST['timeframe'])
        response_data = {}
        response_data['reportbody']=report

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def votecalc(request):
    if request.method == 'POST':
        weight = request.POST.get('weight')
        response_data = {}
        account = Account(str(request.user))
        response_data['weightvalue']=round(account.get_voting_value_SBD(int(weight)),3)
        response_data['weight']=str(weight)

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )


def index(request):
    context = {}
    template = loader.get_template('activities/index.html')
    context['blogs'], context['resteems'] = utils.frontPage()
    context['avatar'] = utils.get_profile(request.user.username)
    context['leaders'] = stats.XhstStats('exhaust').frontpage_leaders()
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()

    return HttpResponse(template.render(context,request))

def article(request, tag, author, permlink):
    context = {}
    context['upslider']=UpvoteForm()
    context['downslider']=DownvoteForm()
    context['commentform']=CommentForm()
    template = loader.get_template('activities/article.html')
    response = utils.get_article(request,author,permlink)
    context['author'] = response[0]
    context['soup'] = response[1]
    context['title'] = response[2]
    context['downvotes'] = response[3]
    context['upvotes'] = response[4]
    context['payout'] = response[5]
    context['replies'] = response[6]
    context['shares'] = response[7]
    context['voted']=response[8]
    context['commenters']=response[9]
    context['permlink']=response[10]
    context['stamp']=int(datetime.now().timestamp())
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()

    return HttpResponse(template.render(context,request))

def blog(request):
    context = {}
    template = loader.get_template('activities/blog.html')
    context['titleform']=TitleForm()
    context['postform'] = PostForm()
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()
    context['tagform']=TagForm({'tags':'blog, fitnation,'})
    context['stamp']=int(datetime.now().timestamp())
    return HttpResponse(template.render(context,request))


def dash(request):
    context = {}
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()
    template = loader.get_template('activities/dashboard.html')
    user_settings, created = Setting.objects.get_or_create(user=request.user)
    setting_form = SettingForm(instance=user_settings)

    strava_url, state = keychain_utils.get_strava_url()
    polar_url, state = keychain_utils.get_polar_url()
    context['user_settings']=setting_form
    context['strava'] = strava_url
    context['polar'] = polar_url
    dist_chart_running, dist_chart_cycling, dist_chart_hiking = dashboard.get_distance_charts(request.user.username)

    context['charts'] = {}
    context['charts']['running']=dist_chart_running
    context['charts']['cycling']=dist_chart_cycling
    context['charts']['hiking']=dist_chart_hiking
    context['reportform'] = PostForm()
#    }, ## repeat for other stat types
    if request.method=="POST":
        update = SettingForm(request.POST, instance=user_settings)
        update.save()
        return HttpResponseRedirect('/dashboard/')

    return HttpResponse(template.render(context,request))

def leaders(request):
    context = {}
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()
    running = activityType.objects.get(type="running")
    cycling = activityType.objects.get(type="cycling")
    hiking = activityType.objects.get(type="hiking")
    context['runleadersWTD'] = leader_chart(running, "WTDdist")
    context['cycleleadersWTD'] = leader_chart(cycling, "WTDdist")
    context['hikeleadersWTD'] = leader_chart(hiking, "WTDdist")
    context['runleadersMTD'] = leader_chart(running, "MTDdist")
    context['cycleleadersMTD'] = leader_chart(cycling, "MTDdist")
    context['hikeleadersMTD'] = leader_chart(hiking, "MTDdist")
    context['runleadersYTD'] = leader_chart(running, "YTDdist")
    context['cycleleadersYTD'] = leader_chart(cycling, "YTDdist")
    context['hikeleadersYTD'] = leader_chart(hiking, "YTDdist")
    context['runleadersALL'] = leader_chart(running, "ALLdist")
    context['cycleleadersALL'] = leader_chart(cycling, "ALLdist")
    context['hikeleadersALL'] = leader_chart(hiking, "ALLdist")
    template = loader.get_template('activities/leaderboard.html')
    return HttpResponse(template.render(context,request))


def feed(request, atype):
    context = {}
    template = loader.get_template('activities/feed.html')
    account = Account(request.user.username)
#    print(request.user.username)
    following = User.objects.filter(username__in=account.get_following())
    full_feed = utils.fetch_feed(atype)
    context['feed_recent']=full_feed
    context['feed_distance']=full_feed.order_by('distance').reverse()
    context['feed_underapp']=full_feed.order_by('rewards')
    context['feed_following_recent']=full_feed.filter(user__in=following)
    context['feed_following_distance']=full_feed.filter(user__in=following).order_by('distance').reverse()
    context['feed_following_underapp']=full_feed.filter(user__in=following).order_by('rewards')
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()
    context['stamp']=int(datetime.now().timestamp())
    return HttpResponse(template.render(context,request))

@login_required
def upload(request):
    context = {}
    user = request.user
    context['strava'] = Strava_Token.objects.filter(user=user).count()
    context['polarflow'] = Polarflow_Token.objects.filter(user=user).count()
    context['manualgps'] = ActivityGPSManual()
    context['acttype'] = ActivityTypeForm()
    context['filegps'] = ActivityGPSFile()
    context['types'] = activityType.objects.all()
    template = loader.get_template('activities/upload.html')
    if request.method=='POST':
        if 'gpsfile' in request.FILES:
            act = Activity(user=user, type=activityType.objects.get(pk=request.POST['category']), title=request.POST['title'], gpsfile=request.FILES['gpsfile'])
            if 'photo' in request.FILES:
                act.photo = request.FILES['photo']
            else:
                act.photo.name = str(act.type.type)+"-default.jpg"
            if 'photo2' in request.FILES:
                act.photo2 = request.FILES['photo2']
            if 'photo3' in request.FILES:
                act.photo3 = request.FILES['photo3']
            act.save()
            return HttpResponseRedirect('/activities/'+str(act.pk)+'/')
        else:
            act = Activity(user=user, type=activityType.objects.get(pk=request.POST['category']), title=request.POST['title'], distance=request.POST['distance'],duration=request.POST['duration'])
            if 'photo' in request.FILES:
                act.photo = request.FILES['photo']
            else:
                act.photo.name = str(act.type.type)+"-default.jpg"
            if 'photo2' in request.FILES:
                act.photo2 = request.FILES['photo2']
            if 'photo3' in request.FILES:
                act.photo3 = request.FILES['photo3']
            act.save()
            return HttpResponseRedirect('/activities/'+str(act.pk)+'/')
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()
    return HttpResponse(template.render(context,request))

def strava(request):
    act = utils.get_strava_activities(request.user)
    if act == 'null':
        return HttpResponse("No new activities found in your Strava!")
    return HttpResponseRedirect('/activities/'+str(act.pk)+'/')

def profile(request, username):
    context = {}
    context['username'] = username
    context['account'], context['activities'], context['following']=utils.fetch_profile(username, request.user.username)
    context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()
    template = loader.get_template('activities/xhst_profile.html')
    return HttpResponse(template.render(context,request))


def activity_detail(request, activity):
    context = {}
    act = Activity.objects.get(pk=activity)
#    act.comments = utils.mentioner(act.comments)
    template = loader.get_template('activities/activity_detail.html')
    distance = 0
    duration = 0
    pace = 0
    points = []
    start_time = 0
    splits = []
    hr_avg = 0
    hr_max = 0
    climb = 0
    finish_time = 0
    chart_json = []
    map_json = []
    context['activity'] = act
    context['stamp']=int(datetime.now().timestamp())
    if act.photo.name == "":
        act.photo.name = str(act.type.type)+'-default.jpg'
    if "gpx" in act.gpsfile.name:
        distance, duration, pace, points, start_time, splits, notes = importGPX('media/'+act.gpsfile.name)
        chart_json, splits = gpx_chart(points)
        map_json = gpx_map(points)
        if not act.posted:
            act.distance = int(round(distance))
            act.duration = int(duration)
            act.pace = round((act.duration/60)/(act.distance/1000),3)
            if act.comments == "":
                if notes == None:
                    act.comments = ""
                else:
                    act.comments = notes
            act.save()
    elif "tcx" in act.gpsfile.name:
        points, pace, hr_avg, hr_max, climb, finish_time, distance, duration, splits, notes = importTCX('media/'+act.gpsfile.name)
        chart_json, splits = tcx_chart(points)
        map_json = tcx_map('media/'+act.gpsfile.name)
        if not act.posted:
            act.distance = int(distance)
            act.duration = duration
            act.pace = round((act.duration/60)/(act.distance/1000),3)
            if act.comments == "":
                if notes == None:
                    act.comments = ""
                else:
                    act.comments = notes
            act.save()
    elif (act.json_data != "") and ('errors' not in act.json_data):
        if "climbing" in act.json_data:
            context['climbing_data'] = eval(act.json_data)[0]
        else:
            chart_json, splits = json_chart(act.json_data)
    else:
        if act.type.type in ['running','cycling','hiking']:
            act.pace = round((act.duration/60)/(act.distance/1000),3)
            act.save()
        elif act.type.type == 'climbing':
            act.pace = 0
            act.save()

    if not act.posted:
        template = loader.get_template('activities/edit_post_activity.html')
        if request.method=="POST":
            update = ActivityForm(request.POST, instance=act)
            update.save()
            print(update)
            print(request.FILES)
            if 'gpsfile' in request.FILES:
                act.gpsfile = request.FILES['gpsfile']
                act.save()
            if 'photo' in request.FILES:
                act.photo = request.FILES['photo']
                act.save()
            if 'photo2' in request.FILES:
                print("YES!")
                act.photo2 = request.FILES['photo2']
                act.save()
            if 'photo3' in request.FILES:
                act.photo3 = request.FILES['photo3']
                act.save()

            return HttpResponseRedirect('/activities/'+str(act.pk)+'/')
        else:
            post_body = autoposter.simple_activity(act)
            context['chartJSON'] = chart_json
            context['postform']=PostForm({'post_body':post_body})
            context['tagform']=TagForm({'tags':'exhaust,'+act.type.type+',fitnation'})
            context['activityform']=ActivityForm(instance=act)
            print(act.date, act.start)
            return HttpResponse(template.render(context,request))
    else:
        if request.method=="POST":
            update = ActivityForm(request.POST, instance=act)
            update.save()
            print(update)
            print(request.FILES)
            if 'gpsfile' in request.FILES:
                act.gpsfile = request.FILES['gpsfile']
                act.save()
            if 'photo' in request.FILES: 
                act.photo = request.FILES['photo']
                act.save()
            if 'photo2' in request.FILES:
                print("YES!")
                act.photo2 = request.FILES['photo2']
                act.save()
            if 'photo3' in request.FILES:
                act.photo3 = request.FILES['photo3']
                act.save()

            return HttpResponseRedirect('/activities/'+str(act.pk)+'/')
        else:
            post_body = autoposter.simple_activity(act)
            context['postform']=PostForm({'post_body':post_body})
            context['activityform']=ActivityForm(instance=act)

        print("===============================")
        context['upslider']=UpvoteForm()
        context['downslider']=DownvoteForm()
        context['commentform']=CommentForm()
        response = utils.activity_steem_performance(request, act.permlink)
        context['author'] = response[0]
        context['soup'] = response[1]
        context['title'] = response[2]
        context['downvotes'] = response[3]
        context['upvotes'] = response[4]
        context['payout'] = response[5]
        context['replies'] = response[6]
        context['shares'] = response[7]
        context['voted']=response[8]
        context['commenters']=response[9]
        context['permlink'] = response[10]
        try:
            act.rewards = response[5]
            if act.rewards == 'null':
                act.rewards=0
        except:
            pass
        act.save()
        context['chartJSON'] = chart_json
        context['mapJSON'] = map_json
        context['splits']=splits
        context['newsletters'] = Newsletter.objects.all().order_by('uploaded_at').reverse()
        return HttpResponse(template.render(context,request))

class ActivityViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

class Logout(View):
    error = "There was an unexpected error while exiting"
    success = "See you again {}"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            ms.success(request,self.success.format(request.user))
            logout(request)
        return HttpResponseRedirect("/")

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'activities': reverse('activity-list', request=request, format=format)
    })

