from activities.models import Activity, activityType

from django.contrib.auth.models import User

from rest_framework import serializers

import json


class ActivitySerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')
#    activity_id = serializers.HyperlinkedIdentityField(view_name='activity-detail', format='html')
    class Meta:
        model = Activity
        fields = ('id', 'user', 'title', 'distance', 'duration', 'type')

    def create(self, validated_data):
#        request = self.context['request']
        print(validated_data)
        request = self.context['request']
        print(request, request.data)
        if 'gpsfile' in validated_data:
#            gpsfile = validated_data['gpsfile']
#            gpsfile = gpsfile.file
            return Activity.objects.create(user=validated_data['owner'], title=validated_data['title'], gpsfile=(validated_data['gpsfile']),type=validated_data['type'])
        else:
            return Activity.objects.create(user=validated_data['owner'], title=validated_data['title'], distance=validated_data['distance'],duration=validated_data['duration'],type=validated_data['type'])


class UserSerializer(serializers.HyperlinkedModelSerializer):
    activities = serializers.HyperlinkedRelatedField(many=True, view_name="activity-detail", read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'activities')
