from django.conf.urls import url, include
from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from . import views

activity_list = views.ActivityViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
activity_detail = views.ActivityViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
user_list = views.UserViewSet.as_view({
    'get': 'list'
})
user_detail = views.UserViewSet.as_view({
    'get': 'retrieve'
})

urlpatterns = [
    path('', views.index, name='index'),
    path('<str:tag>/@<str:author>/<str:permlink>', views.article, name='article'),
    path('blog/', views.blog, name='blog'),
    path('dashboard/', views.dash, name='dash'),
    path('activities/<int:activity>/', views.activity_detail, name='detailview'),
    path('feed/<str:atype>/', views.feed, name='feed'),
    path('leaderboards/', views.leaders, name='leaders'),
    path('@<str:username>/', views.profile, name='profile'),
    path('api/v1/', views.api_root),
    path('api/v1/activities/', activity_list, name='activity-list'),
    path('api/v1/activities/<int:pk>/', activity_detail, name='activity-detail'),
    path('api/v1/users/', user_list, name='user-list'),
    path('api/v1/users/<int:pk>/', user_detail, name='user-detail'),
    path('delete/', views.delete_activity, name='delete'),
    path('logout/',views.Logout.as_view()),
    path('report/', views.fetchreport),
    path('upload/', views.upload, name='upload'),
    path('upload/strava/', views.strava, name='strava'),
    path('votecalc/',views.votecalc, name="fetch-account-data"),
]

urlpatterns = format_suffix_patterns(urlpatterns,allowed=['json','html'])
