from activities.models import Activity, activityType
#from activities.views import check_tax
from datetime import datetime
from django.contrib.auth.models import User
from beem.account import Account
from beem.comment import Comment
from beem.steem import Steem
import re



def weekly_payout():
    s = Steem()
    exhaust = Account('exhaust')
    exhaust.claim_reward_balance()
    paypool = {}
    paypool['total'] = 0.0
    paylist = {}
    paylist['climbing']={}
    paylist['climbing']['total']=0.0
    paylist['cycling']={}
    paylist['cycling']['total']=0.0
    paylist['hiking']={}
    paylist['hiking']['total']=0.0
    paylist['running']={}
    paylist['running']['total']=0.0
    paylist['yoga']={}
    paylist['yoga']['total']=0.0
    paylist['strength'] = {}
    paylist['strength']['total']=0.0
    paylist['swimming'] = {}
    paylist['swimming']['total']= 0.0
    userlist = {}
    now = datetime.now().timestamp()
#    now = datetime(2019,9,8,10,0).timestamp()
    oneweek = datetime.fromtimestamp(now-60*60*24*7)
    twoweek = datetime.fromtimestamp(now-60*60*24*7*2)
    allacts = Activity.objects.filter(uploaded_at__gte=twoweek,uploaded_at__lte=oneweek, posted=True)
    print("Creating list of users to be paid")
    for act in allacts:
        print(act.title, act.user.username, act.distance)
        paylist[act.type.type][act.user.username]=0.0
    acttypes = activityType.objects.all()
    for acttype in acttypes:
        paypool[acttype.type]=0.0
        acts = allacts.filter(type=acttype)
        for act in acts:
            user = User.objects.get(username=act.user.username)
            if user.username in userlist:
                pass
            else:
                userlist[user.username]={}
                userlist[user.username]['running']=0.0
                userlist[user.username]['cycling']=0.0
                userlist[user.username]['hiking']=0.0
                userlist[user.username]['yoga']=0.0
                userlist[user.username]['strength']=0.0
                userlist[user.username]['swimming']=0.0
            #taxes, events = check_tax(user,acttype)
            comment = Comment(act.permlink)
#            payamt = (comment.get_rewards()['author_payout']*0.1).amount
            payamt = comment.reward.amount*0.5*0.1
            paypool[acttype.type]+=payamt
            if act.type.type=="yoga" or act.type.type=="strength":
                paylist[acttype.type][act.user.username]+=act.duration/60
            else:
                paylist[acttype.type][act.user.username]+=act.distance/1000
            if act.type.type=="yoga" or act.type.type=="strength":
                paylist[acttype.type]['total']+=act.duration/60
            else:
                paylist[acttype.type]['total']+=act.distance/1000
        paypool['total']+=paypool[acttype.type]
    print(paypool)
    exhaust = Account('exhaust')
    print(exhaust.get_balances()['rewards'])
    for user in userlist:
#        skiplist = ['mackmck','dejan.vuckovic','browery']
#        if user in skiplist:
#            continue
        paytotal = 0.0
        #taxes = 0
        events = []
        userlist[user]['running_holdback'] = 0
        for act in userlist[user]:
            events = []
            if act == "running_holdback":
                continue
            if user in paylist[act]:
                #taxes, events = check_tax(User.objects.get(username=user),activityType.objects.get(type=act))
                amt_earned = round(paypool[act]*paylist[act][user]/paylist[act]['total'],3)
                userlist[user][act] = amt_earned
                paytotal += (amt_earned) #.value
#            if len(events) >= 1:
#                userlist[user]['running_holdback'] = (taxes/2 - 0.075)/0.075*userlist[user]['running']
        userlist[user]['total'] = paytotal
#        userlist[user]['total'] = paytotal * 2 # uncommen this line to add a multiplier to payout
        paymsg = ("Way to go, @%s! You have earned %.3f SBD from getting EXHAUSTED last week!" % (user, userlist[user]['total']))
        if userlist[user]["running"]>0.0:
            paymsg += (" %.3f SBD was earned from running." % (userlist[user]['running'])) 
        if userlist[user]["cycling"]>0.0:
            paymsg += (" %.3f SBD was earned from cycling." % (userlist[user]['cycling'])) 
        if userlist[user]["yoga"]>0.0:
            paymsg += (" %.3f SBD was earned from yoga." % (userlist[user]['yoga'])) 
        if userlist[user]["hiking"]>0.0:
            paymsg += (" %.3f SBD was earned from hiking." % (userlist[user]['hiking'])) 
        if userlist[user]["strength"]>0.0:
            paymsg += (" %.3f SBD was earned from strength training." % (userlist[user]['strength']))
        if userlist[user]["swimming"]>0.0:
            paymsg += (" %.3f SBD was earned from swim training." % (userlist[user]['swimming']))
        payamt = round(userlist[user]['total'],3)
#        paymsg += ". We ended up doubling the rewards this week, just because bigger numbers are more exciting!"  # Uncomment this line for special messages to posts, such as multipliers
        exhaust.transfer(user,payamt, asset='SBD', memo=paymsg, account=exhaust)
        print(paymsg)
    mathtotal = 0.0
    for user in userlist:
        mathtotal += userlist[user]['total']
    print(round(mathtotal,3))
