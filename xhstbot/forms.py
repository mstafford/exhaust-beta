from xhstbot.models import Setting
from django import forms
from django.forms import ModelForm
from markdownx.fields import MarkdownxFormField

class SettingForm(forms.ModelForm):
    class Meta:
        model = Setting
        fields = ('map_privacy','auto_scrape_sites','auto_post_acts', 'auto_vote', 'vote_all', 'delegator_donate','delegator_donate_pct')
        help_texts = {
            'map_privacy': 'Automatically share maps if set to true.',
            'auto_scrape_sites':'Automatically check synced accounts for new activity uploads.',
            'auto_post_acts':'Automatically post to Steem for every new activity using default post description, or a custom template of your own.',
            'auto_vote':"Allow @exhaust to cast votes for me when new activities are published by athletes I've whitelisted",
            'vote_all':"If true, @exhaust will cast your upvotes for all activities that are posted",
            'delegator_donate':"If true, and you're delegating to @exhaust, you can define a percentage of your rewards to be donated to @exhaust to help the project grow even faster! No pressure though!",
            'delegator_donate_pct':"Specify the percent of your earned curation rewards you'd like to donate",
        }
