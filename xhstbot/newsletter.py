from activities.autoposter import newsletter as get_newsletter
from activities import utils
#from activities.models import Stat, activityType
#from activities.models import Activity
from beem.account import Account
from beem.comment import Comment
from django.contrib.auth.models import User

from datetime import datetime
#from steem import Steem
from beem import Steem as Beem
import re
import time

def daily_newsletter():
    utils.update_newsletters()
    newsletter = get_newsletter()
    today = datetime.today()
    title = "State of Exhaustion - " + today.strftime("%B %d, %Y")
    tags = ['exhaust','fitnation','running','cycling','palnet','newsletter', 'sportstalk']
    steem = Beem()
    steem.post(title=title, body=newsletter, author='exhaust', tags=tags)
