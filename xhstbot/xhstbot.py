from beem.blockchain import Blockchain
from django.contrib.auth.models import User

from .models import RewardPool
import json

class XHSTbot():

    chain = Blockchain()
    users = User.objects.all()
    userlist = []
    for user in users:
        userlist.append(user.username)

    def xhstStart(self):
        for block in self.chain.blocks():
            for trx in block.transactions:
                if (trx['operations'][0]['type'] == 'comment_operation') and ('app' in trx['operations'][0]['value']['json_metadata']):
                    json_data = json.loads(trx['operations'][0]['value']['json_metadata'])
                    if 'exhaust' in json_data['app']:
                        print("Found comment coming from exhaust!")
                        print(trx['operations'][0])

#                if (trx['operations'][0]['type'] == 'comment_operation' and trx['operations'][0]['value']['author'] in self.userlist) or (trx['operations'][0]['type'] == 'vote_operation' and trx['operations'][0]['value']['voter'] in self.userlist):
#                    print("===================================")
    
