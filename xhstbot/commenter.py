from django.contrib.auth.models import User

from activities.models import Activity, activityType
from beem.comment import Comment
from beem.steem import Steem
from datetime import datetime
from exhaust import secret

import time


def sport_summary(activity):
    now = datetime.now().timestamp()
    user = activity.user
    acttype = activity.type
    total_distance = 0.0
    total_duration = 0.0
    acts = Activity.objects.filter(user=user, type=acttype)
    latest = acts.last()
    similar_acts = acts.filter(distance__gte=latest.distance*0.9, distance__lte=latest.distance*1.1)
    similar_acts = similar_acts.order_by('pace')
    if len(similar_acts) > 1:
        xhstComment = ("You have uploaded %d %s activities, and %d of them have been of similar distances (within 10 percent)!\n" % (len(acts),acttype.type,len(similar_acts)))
        xhstComment += ("Here are your 5-fastest %s activities of a similar distance:\n" % (acttype.type))
        xhstComment += "\n"
        for act in similar_acts[:5]:
            minutes = int(float(act.pace))
            seconds = int((float(act.pace)-minutes)*60)
            pace = str(minutes)+":"+str(seconds).rjust(2,'0')
            xhstComment += " - " + "["+act.title+"](https://xhaust.me/activities/"+str(act.pk)+")" + " - " + str(act.distance/1000) + "km @ " + pace + " min/km pace.\n"
        for act in similar_acts:
            total_distance += act.distance/1000
            total_duration += act.duration/60
        avg_pace = (total_duration/total_distance)
        average_pace = str(int(avg_pace)) + ":" + str(int((avg_pace-int(avg_pace)) *60)).rjust(2,'0')

        xhstComment += ("\nOver these %d similar activities, you have travelled approximately %.2f kms at an average pace of roughly %s min/km!\n" % (len(similar_acts),total_distance, average_pace))
        latestpace = (latest.duration/60)/(latest.distance/1000)
        xhstComment += "Your latest run was at a pace of approximately " + str(activity.pace) + "min/km!"
        if latestpace >= avg_pace:
            xhstComment+="\nLooks like you went a bit slower this time! Remember that it's important to take some rest days, and do some rehab / prehab / strength and conditioning training too! If you weren't intending to go slower, take a moment to listen to your body, and see if you need something. Check in with yourself!"
        else:
            xhstComment+="\nLooks like you went a bit faster than usual this time! Nice work!\nHave a !BEER for your troubles!\n"
    else:
        xhstComment = ("This is the first activity you've shared in this distance range! Post a few more, and we'll have some historic data to compare with! Keep getting EXHAUSTED!")
    print(xhstComment)
    steem = Steem(node="https://api.steemit.com")
    print(activity.permlink)
    comment = Comment(activity.permlink, steem_instance=steem, use_tags_api=False)
    steem.wallet.unlock(secret.WALLET_UNLOCK)
    comment.reply(xhstComment, title=comment.title + " - XHST Review",author="exhaust")
#    time.sleep(3)
#    print("Posted?")
#    print(latest.title, latest.distance)
#    print(similar_acts)
