#from activities.commenter import sport_summary
#from activities.models import Action
#from steemvoter.claim import check_RC
from activities.models import Stat, Activity
from activities import utils
from activities.stats import XhstStats
from django.contrib.auth.models import User
from keychain.models import Strava_Token
from keychain import views

from beem import Steem
from beem.account import Account
from beem.comment import Comment
from datetime import datetime
from xhstbot.models import Commenter, AutoVoter, Setting

import re

def refresh_stats():
    xhst = XhstStats('exhaust')
    xhst.update_all_users()

def voter():
    queueList = AutoVoter.objects.all().order_by('post_date')
#    commentList = Commenter.objects.all()
    now = round(datetime.now().timestamp())
    comment_delay = 60
    vote_delay = 120
    weight = 100
#    for comment in commentList:
#        if now - comment.activity.postdate.timestamp() >= comment_delay:
#            sport_summary(comment.activity)
#            comment.delete()
    for queue in queueList:
        print("Checking" + str(queue.permlink))
        if now - queue.post_date.timestamp() >= queue.vote_delay:
            if "comment" in queue.permlink:
                weight = 25
                comment=Comment("@"+queue.user.username+"/"+queue.permlink)
                comment.vote(weight,'exhaust')
                queue.delete()
            else:
                comment=Comment("@"+queue.user.username+"/"+queue.permlink)
                views.auto_vote("@"+queue.user.username+"/"+queue.permlink)
                comment.vote(weight,'exhaust')
                queue.delete()
#            print("Voted on a post by @"+post.author)

def commentvoter():
    account = Account('exhaust')
    VP = account.get_voting_power()
    weekago = datetime.now().timestamp() - 7*3600*24
#    latest = Action.objects.filter(postdate>=weekago)
    if VP >= 92.0:
        print("We should be adding a comment into the vote queue here!")

def stravascrape():
    strava_users = Strava_Token.objects.all()
    for strava in strava_users:
        user = strava.user
        print("Checking for new activity from " + user.username)
        new_act = utils.get_strava_activities(user)
        if new_act == "null":
            print("Nothing new from " + user.username)
        else:
            print("Found and added new activity from " + user.username + "!")
            user_settings = Setting.objects.get(user=user)
            if (user_settings.auto_post_acts):
                act = Activity.objects.filter(user=user,posted=False).last()
                views.auto_post(act)


def pay_delegators(): ## TO BE PHASED OUT FOR TRANSACTION BUILDER
    exhaust = Account('exhaust')
    hist = exhaust.history(only_ops=['delegate_vesting_shares'])
    delegators = {}
    s = Steem()
#    b = Beem()
    curation_rewards = exhaust.curation_stats()['24hr']
    now = datetime.now()
    sevenday = datetime.fromtimestamp(now.timestamp()-7*24*3600)
    effective_vests = exhaust.get_vests().amount
#    donate_list = ['mstafford','runburgundy','jkms','caitycat','harryp3','run.vince.run','ervin-lemark','urbangladiator']
    donate_list = []
    # Find all active delegations:
    for h in hist:
#        print(h)
        timestamp = datetime.strptime(h['timestamp'],'%Y-%m-%dT%H:%M:%S')
        if h['delegator'] != 'exhaust' and timestamp <= sevenday:
            delegators[h['delegator']] = {}
            delegators[h['delegator']]['amount']=h['vesting_shares']['amount']
    # Calculate percentages for each user:
    for delegator in delegators:
        user, created = User.objects.get_or_create(username=delegator)
        user_settings, created = Setting.objects.get_or_create(user=user)
        if int(delegators[delegator]['amount']) <= 0:
            continue
        if user_settings.delegator_donate:
            delegators[delegator]['percentage'] = (int(delegators[delegator]['amount'])/1000000)/effective_vests
            usershare = round(delegators[delegator]['percentage']*float(curation_rewards),3) * (1-user_settings.delegator_donate_pct/100) + 0.001
            memo="Thanks for delegating and donating to Exhaust, " +str(delegator) +"! You have delegated " + str(round(s.vests_to_sp(float(delegators[delegator]['amount'])/1000000),3)) + " SP! Here is your portion of the curation rewards earned yesterday after generously donating " + str(user_settings.delegator_donate_pct) + "% of your share! You will be eternally remembered as a hero of the blockchain!"
#            print(memo, usershare)
            exhaust.transfer(to=delegator, amount = usershare, asset='STEEM', memo=memo, account='exhaust')
            continue
        delegators[delegator]['percentage'] = (int(delegators[delegator]['amount'])/1000000)/effective_vests
        usershare = round(delegators[delegator]['percentage']*float(curation_rewards),3)
        memo="Thanks for delegating to Exhaust, " +str(delegator) +"! You have delegated " + str(round(s.vests_to_sp(float(delegators[delegator]['amount'])/1000000),3)) + " SP! Here is your portion of the curation rewards earned yesterday!"
        exhaust.transfer(to=delegator, amount = usershare, asset='STEEM', memo=memo, account='exhaust')

    def claim_account():
        check_RC()
