from django.contrib import admin

from .models import RewardPool, AutoVoter, Setting

admin.site.register(RewardPool)
admin.site.register(AutoVoter)
admin.site.register(Setting)
