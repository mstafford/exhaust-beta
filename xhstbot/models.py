from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from activities.models import Activity

# Create your models here.

class RewardPool(models.Model):
    pool_name = models.CharField(max_length=32, blank=True)
    last_block = models.IntegerField(default=0)
    pool_rewards = models.FloatField(default=0.0)
    xshares = models.FloatField(default=0.0)
    def __str__(self):
        return self.pool_name

class TrackedPosts(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    permlink = models.CharField(max_length=128, blank=True)
    xshares = models.FloatField(default=0.0)
    def __str__(self):
        return (self.user.username + " - " + self.permlink)

class AutoVoter(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    permlink = models.CharField(max_length=128, blank=False)
    weight = models.IntegerField(blank=False,default=100)
    vote_delay = models.IntegerField(blank=False, default=300) #delay in seconds, default = 5minutes
    post_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        now = int(datetime.now().timestamp())
        timeleft = self.vote_delay - (now - (int(self.post_date.timestamp())))
        return ("@"+str(self.user.username) + "/" + str(self.permlink) + " - " +str(timeleft)+" seconds away")

class Commenter(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)

class Setting(models.Model):
    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    map_privacy = models.BooleanField(default=False)
    auto_scrape_sites = models.BooleanField(default=False)
    auto_post_acts = models.BooleanField(default=False)
    auto_vote = models.BooleanField(default=False)
    vote_all = models.BooleanField(default=False)
    vote_list = models.ManyToManyField(User, related_name="auto_vote_list")
    custom_default_post = models.TextField(blank=True)
    delegator_donate = models.BooleanField(default=False)
    delegator_donate_pct = models.IntegerField(default=0)
    def __str__(self):
        return("@"+str(self.user.username))
