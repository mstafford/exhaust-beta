from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class SC_Token(models.Model):
    access_token = models.CharField(blank=True, max_length=1024, default='')
    refresh_token = models.CharField(blank=True, max_length=1024, default='')
    user = models.OneToOneField(User, unique = True, on_delete=models.CASCADE)
    age = models.IntegerField(blank=True, default=0)
    def __str__(self):
        return (str(self.user.username))

class Strava_Token(models.Model):
    access_token = models.CharField(blank=True, max_length=1024, default='')
    refresh_token = models.CharField(blank=True, max_length=1024, default='')
    user = models.OneToOneField(User, unique = True, on_delete=models.CASCADE)
    code = models.CharField(blank=True, max_length=1024, default='')
    last_act = models.CharField(blank=True, max_length=16, default='')
    def __str__(self):
        return (str(self.user.username))

class Polarflow_Token(models.Model):
    access_token = models.CharField(blank=True, max_length=1024, default='')
    refresh_token = models.CharField(blank=True, max_length=1024, default='')
    user = models.OneToOneField(User, unique = True, on_delete=models.CASCADE)
    code = models.CharField(blank=True, max_length=1024, default='')
    def __str__(self):
        return (str(self.user.username))


