from django.contrib import admin
from .models import SC_Token, Strava_Token, Polarflow_Token
# Register your models here.

admin.site.register(SC_Token)
admin.site.register(Strava_Token)
admin.site.register(Polarflow_Token)
