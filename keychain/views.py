from activities.autoposter import simple_activity
from activities.models import Activity, UserProfile
from beem.account import Account

from datetime import datetime

from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views import View
from django.conf import settings
from django.template import loader

from keychain.models import SC_Token, Strava_Token, Polarflow_Token

from steemconnect.client import Client
from steemconnect.operations import Comment, CommentOptions, Vote

from xhstbot import commenter
from xhstbot.models import AutoVoter, Setting

import json
import re
# Create your views here.

def comment(request): ## endpoint for AJAX steemconnect vote-operations
    if request.method == 'POST':
        comment_user = request.POST.get('username')
        comment_body = request.POST.get('comment_body')
        comment_title = request.POST.get('comment_title')
        comment_permlink = request.POST.get('comment_permlink')
        comment_parent_permlink = request.POST.get('comment_parent_permlink')
        comment_parent_author = request.POST.get('comment_parent_author')

        response_data = {}
        user = User.objects.get(username=comment_user)
        client = Client(client_id=settings.CLIENT_ID,client_secret=settings.SC_SECRET, access_token = user.sc_token.access_token)
        comment = Comment(comment_user,comment_permlink,comment_body,comment_title,comment_parent_author, comment_parent_permlink)
        benef = [{'account': 'exhaust', 'weight': 500}]
        comment_options = CommentOptions(parent_comment=comment, percent_steem_dollars=10000, extensions=[[0,{'beneficiaries':benef}]])
        comment_ops = comment.to_operation_structure()
        options_ops = comment_options.to_operation_structure()
        response_data = client.broadcast([comment_ops, options_ops])

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def create_post(request): ## endpoint for AJAX steemconnect post-operations
    if request.method == 'POST':
        comment_user = request.POST.get('username')
        comment_body = request.POST.get('comment_body')
        comment_title = request.POST.get('comment_title')
        comment_permlink = request.POST.get('comment_permlink')
        comment_parent_permlink = 'exhaust'
        comment_parent_author = ''
        json_metadata = {'app':'exhaust-beta/0.2b','tags':request.POST.get('tags')}

        response_data = {}
        user = User.objects.get(username=comment_user)
        client = Client(client_id=settings.CLIENT_ID,client_secret=settings.SC_SECRET, access_token = user.sc_token.access_token)
        comment = Comment(comment_user,comment_permlink,comment_body,comment_title,comment_parent_author, comment_parent_permlink,json_metadata=json_metadata)
        benef = [{'account': 'exhaust', 'weight': 2000}]
        comment_options = CommentOptions(parent_comment=comment, percent_steem_dollars=10000, extensions=[[0,{'beneficiaries':benef}]])
        comment_ops = comment.to_operation_structure()
        options_ops = comment_options.to_operation_structure()
        response_data = client.broadcast([comment_ops,options_ops])

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def auto_post(act): ## endpoint for AJAX steemconnect post-operations
    comment_user = act.user.username
    comment_body = simple_activity(act)
    comment_title = act.title
    stamp = int(datetime.now().timestamp())
    comment_permlink = 'xhst-activity-'+str(stamp)
    comment_parent_permlink = 'exhaust'
    comment_parent_author = ''
    json_metadata = {'app':'exhaust-beta/0.2b','tags':['exhaust','fitnation',str(act.type.type)]}

    response_data = {}
    user = User.objects.get(username=comment_user)
    client = Client(client_id=settings.CLIENT_ID,client_secret=settings.SC_SECRET, access_token = user.sc_token.access_token)
    comment = Comment(comment_user,comment_permlink,comment_body,comment_title,comment_parent_author, comment_parent_permlink,json_metadata=json_metadata)
    benef = [{'account': 'exhaust', 'weight': 2000}]
    comment_options = CommentOptions(parent_comment=comment, percent_steem_dollars=10000, extensions=[[0,{'beneficiaries':benef}]])
    comment_ops = comment.to_operation_structure()
    options_ops = comment_options.to_operation_structure()
    response_data = client.broadcast([comment_ops,options_ops])
    print(response_data)

    vote_queue = AutoVoter(user=user,permlink=comment_permlink)
    vote_queue.save()
    act.posted = True
    act.permlink = "@"+str(user.username)+"/"+str(comment_permlink)
    act.save()
    if act.distance > 0:
        commenter.sport_summary(act)

    response = json.dumps(response_data)

    return(response)



def commentPost(act):
    if act.distance > 0:
        commenter.sport_summary(act)

def queueVote(request):
    if request.method == 'POST':
        comment_user = request.POST.get('username')
        comment_permlink = request.POST.get('comment_permlink')
        #weight = request.POST.get('weight')
        weight=100
        response_data = {}
        user = User.objects.get(username=comment_user)
        if "xhst-comment" not in comment_permlink:
            act = Activity.objects.get(pk=request.POST.get('act_id'))
            act.posted = True
            act.permlink = "@"+comment_user+"/"+comment_permlink
            act.save()
            if act.distance > 0:
                commenter.sport_summary(act)
                print("Commenting on new activity!")
        print("Adding activity to vote queue!")
        votequeue, created = AutoVoter.objects.get_or_create(user=user, permlink=comment_permlink,weight=weight)
        votequeue.save()
        if created:
            response_data['response']='success'
        else:
            response_data['error']

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )


def vote(request):
    if request.method == 'POST':
        weight = int(request.POST.get('weight'))/100
        voter = request.POST.get('voter')
        author = request.POST.get('author')
        permlink = request.POST.get('permlink')

        response_data = {}
        user = User.objects.get(username=voter)
        client = Client(client_id=settings.CLIENT_ID,client_secret=settings.SC_SECRET, access_token = user.sc_token.access_token)
        vote = Vote(voter,author,permlink,weight)
        vote_ops = vote.to_operation_structure()
        response_data = client.broadcast([vote_ops])

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def auto_vote(permlink):
    weight = 100
    voters = Setting.objects.filter(auto_vote=True)
    print("Voter list: " + str(voters))
    author = re.search("@.+\/",permlink)[0]
    permlink = re.sub(author,'',permlink)
    author = re.sub('/','',re.sub('@','',author))
    response_data = {}
    vote_ops = []
    for voter in voters:
        print("Attempting to cast vote for " + voter.user.username)
        user = User.objects.get(username=voter.user.username)
        client = Client(client_id=settings.CLIENT_ID,client_secret=settings.SC_SECRET, access_token = user.sc_token.access_token)
        vote = Vote(user.username,author,permlink,weight)
        vote_ops = vote.to_operation_structure()
        response_data = client.broadcast([vote_ops])
        print(response_data)

    #return HttpResponse(
    #    json.dumps(response_data),
    #    content_type="application/json"
    #)

def main(request):
    context = {}
    template = loader.get_template('keychain/main.html')

    return HttpResponse(template.render(context,request))


def LoginSignupSK(request):
    try:
        user = User.objects.get_or_create(username=request.POST['user'])[0]
        profile, created = UserProfile.objects.get_or_create(user=user)
        account = Account(user.username)
        try:
            profile.avatar = account.json_metadata['profile']['profile_image']
            profile.save()
        except:
            profile.avatar = 'https://xhaust.me/media/XHSTlogo.png'
            profile.save()
        login(request, user, backend="django.contrib.auth.backends.ModelBackend")
        return redirect('/')
    except:
        return HttpResponse("No Dice w/ SteemKeychain")

class LoginSignupSC(View):
    client = Client(client_id = settings.CLIENT_ID,
        client_secret = settings.APP_SECRET
    )
    print("sanity check!")
    def get(self, request, *args, **kwargs):
        code = request.GET["code"]
        username = request.GET["username"]
        tokens = self.client.get_access_token(code)
        access_token = tokens["access_token"]
        refresh_token = tokens["refresh_token"]
        user, created = User.objects.get_or_create(username = username)
        sc_token, created = SC_Token.objects.get_or_create(user=user)
        sc_token.refresh_token = refresh_token
        sc_token.access_token = access_token
        sc_token.age = int(datetime.now().timestamp())
        sc_token.save()
        profile, created = UserProfile.objects.get_or_create(user=user)
        account = Account(user.username)
        try:
            profile.avatar = account.json_metadata['profile']['profile_image']
            profile.save()
        except:
            profile.avatar = "null"
            profile.save()
        login(request,user, backend="django.contrib.auth.backends.ModelBackend")
        return HttpResponseRedirect(settings.LOGIN_REDIRECT)

def strava(request):
    context = {}
    user = request.user
    strava, created = Strava_Token.objects.get_or_create(user=user)
    strava.code = request.GET['code']
    strava.save()
    return HttpResponseRedirect('/dashboard/')

def polarflow(request):
    context = {}
    user = request.user
    polar, created = Polarflow_Token.objects.get_or_create(user=user)
    polar.code = request.GET['code']
    polar.save()
    return HttpResponse(polar)
