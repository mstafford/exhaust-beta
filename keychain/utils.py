from django.contrib.auth.models import User
from keychain.models import Strava_Token, Polarflow_Token
from requests_oauthlib import OAuth2Session

import base64
import json
import requests

def get_strava_url():
    client_id = 36782
    redirect_uri = 'https://xhaust.me/keychain/strava/'
    base_url = 'https://www.strava.com/oauth/authorize'
    scope = 'activity:read_all,profile:read_all'
    oauth = OAuth2Session(client_id, redirect_uri=redirect_uri, scope=scope)
    auth, state = oauth.authorization_url(base_url, approval_prompt='force')
    return auth, state

def get_polar_url():
    client_id = "f073bce4-9350-4e8b-98de-261cf7a3b959"
    redirect_uri = 'https://xhaust.me/keychain/polar/'
    base_url = 'https://flow.polar.com/oauth2/authorization'
    oauth = OAuth2Session(client_id, redirect_uri=redirect_uri)
    auth, state = oauth.authorization_url(base_url, approval_prompt='force')
    return auth, state

def get_strava_token(user):
    client_id = 36782
    redirect_uri = 'https://xhaust.me/keychain/strava/'
    token_url = 'https://www.strava.com/oauth/token'
    scope = 'read'
    strava = Strava_Token.objects.get(user=user)
    client_secret = '35ebcbbfea2e6e44de2b5dddd9883b76bc9b22e0'
    oauth = OAuth2Session(client_id, redirect_uri=redirect_uri, scope=scope)
    response = oauth.fetch_token(token_url,code=strava.code,client_secret=client_secret,include_client_id=True)
    strava.access_token = response['access_token']
    strava.refresh_token = response['refresh_token']
    strava.save()

def get_polar_token(user):
    client_id = "f073bce4-9350-4e8b-98de-261cf7a3b959"
    client_secret = "b3262c97-c03b-4bc8-8af5-88cf2156e242"
    encoded = base64.urlsafe_b64encode("{}:{}".format(client_id,client_secret).encode("UTF-8")).decode('ascii')
    headers = {
        'Authorization': "Basic " + encoded,
        'Content-Type': "application/x-www-form-urlencoded",
        'Accept': "application/json;charset=UTF-8"
    }
    polarflow = Polarflow_Token.objects.get(user=user)
    data = {
    'redirect_uri': 'https://xhaust.me/keychain/polar/',
    'grant_type': "authorization_code",
    'code':polarflow.code
    }
    url = "https://polarremote.com/v2/oauth2/token"

#    scope = 'read'
#    oauth = OAuth2Session(client_id, redirect_uri=redirect_uri)
#    response = oauth.fetch_token(token_url,code=polarflow.code,include_client_id=True, headers=headers)
#    print(token_url, data, headers)
    response = requests.post(url=url, headers=headers, data=data)
    print(response.content)
    if response.status_code == 200:
        content = json.loads(response.content)
        print(content)
        polarflow.access_token = content['access_token']
    else:
        print("Something didn't work quite right")
#    polarflow.refresh_token = response['refresh_token']
    polarflow.save()

