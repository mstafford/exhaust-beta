from django.conf.urls import url, include
from django.urls import path
from django.views.generic.base import RedirectView

from . import views

hardcode = 'https://steemconnect.com/oauth2/authorize?client_id=exhaust.app&redirect_uri=https://xhaust.me/keychain/loginSC/&scope=login,offline,vote,comment,delete_comment,comment_options&response_type=code'

urlpatterns = [
    path('loginSK/', views.LoginSignupSK, name='LoginSignupSK'),
    path('login/', RedirectView.as_view(url=hardcode, permanent=False), name='LoginSignupSC'),
    path('loginSC/', views.LoginSignupSC.as_view(), name="SCRedirect"),
    path('scVote/', views.vote, name="SCvote"),
    path('scComment/', views.comment, name="SCcomment"),
    path('scPost/',views.create_post,name="SCpost"),
    path('queuevote/', views.queueVote, name="queuevote"),
    path('main/', views.main, name="main"),
    path('strava/', views.strava, name="strava"),
    path('polar/', views.polarflow, name="polar"),
]

