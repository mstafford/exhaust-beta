# INSTALL INSTRUCTIONS -- DEPRECATED

The easiest way to get started is to use the included
[Vagrantfile](https://www.vagrantup.com/)
`vagrant up`

Below are some notes on a manual setup:

## Install Python dependencies
```
#Fedora/Redhat:
sudo dnf install @development-tools redhat-rpm-config python3-devel openssl-devel

python3 -m venv venv
pip install --upgrade pip
source venv/bin/activate
pip install -r requirements.txt 
```

On a mac
```
brew install libmagic
```

## Install GeoSpatial Libraries
```
#Fedora/Redhat
sudo dnf install binutils proj-devel gdal

#Ubuntu/Debian
sudo apt-get install binutils libproj-dev gdal-bin

#Mac
brew install gdal
```

## Initialize Database
```
python3 manage.py makemigrations
python3 manage.py migrate
```

## Update floppyforms widgets.py (venv/lib/python3.6/site-packages/floppyforms/widgets.py) from:
```
sed -i 's/{}, \*\*kwargs/{}/g' vagrantenv/lib/python3.6/site-packages/floppyforms/widgets.py 
```

## Check BASE URL in SteemConnect backends.py (venv/lib/python3.6/site-packages/steemconnect/backends.py) (v2 is abandoned)
```
sed -i 's/v2\.//g ' vagrantenv/lib/python3.6/site-packages/steemconnect/backends.py 
```

## Create SuperUser for Django Project
```
python manage.py createsuperuser
# follow prompts
```

## To start Django Server:
```
python manage.py runserver
````
* Open browser to localhost:8000
* To open admin page: localhost:8000/admin (login w/ username:password from *createsuperuser* step above)
