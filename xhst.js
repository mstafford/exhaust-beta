// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue =   decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

// CommentForm

// Update vote on slider timeout:
$('#commenter').on('change', function(event){
    event.preventDefault();
    console.log("Comment Input Detected!")  // sanity check
    $("#post_body").val($('#commenter')[0]['elements'][1]['value']);
//    update_comment_body();
});

// SliderForms

// Update vote on slider timeout:
$('#upvote-slider').on('change', function(event){
    event.preventDefault();
    console.log("Change Detected!")  // sanity check
    get_upvote_value();
});

// AJAX for grabbing vote weight
function get_upvote_value() {
    console.log("Grabbing vote numbers") // sanity check
    console.log($('#id_upvote').val());
    $.ajax({
        url : "/votecalc/", // the endpoint
        type : "POST", // http method
        data : { weight : $('#id_upvote').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#upweight").text(json.weight+"% VOTE = ");
            $("#upvalue").text(json.weightvalue+" SBD");
            $("#upvote_weight").val(json.weight*100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

// Update vote on slider timeout:
$('#dashSummary').on('click', function(event){
    event.preventDefault();
    console.log("Dashboard Summary Requested!");  // sanity check
    document.querySelector("#sidepanelSummary").style.display="block";
    document.querySelector("#summaryFeed").style.display="block";
    document.querySelector("#sidepanelYears").style.display="none";
    document.querySelector("#activityDetail").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="none";
    document.querySelector("#xhstSettings").style.display="none";
});

// Update vote on slider timeout:
$('#dashActivities').on('click', function(event){
    event.preventDefault();
    console.log("Dashboard Activities Requested!")  // sanity check
    document.querySelector("#sidepanelSummary").style.display="none";
    document.querySelector("#summaryFeed").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="none";
    document.querySelector("#xhstSettings").style.display="none";
    document.querySelector("#sidepanelYears").style.display="block";
    document.querySelector("#activityDetail").style.display="block";
});

// Display and Edit Users XHST Settings (API Key)
$('#dashSettings').on('click', function(event){
    event.preventDefault();
    console.log("User Settings Requested!") // sanity check
    document.querySelector("#sidepanelSummary").style.display="none";
    document.querySelector("#summaryFeed").style.display="none";
    document.querySelector("#sidepanelYears").style.display="none";
    document.querySelector("#activityDetail").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="block";
    document.querySelector("#xhstSettings").style.display="block";
    get_user_settings();
});

// AJAX for grabbing vote weight
function get_user_settings() {
    console.log("Retrieving Settings") // sanity check
    $.ajax({
        url : "/usersettings/", // the endpoint
        type : "GET", // http method
        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#xhstAPIKey").text(json.apikey);
            $("#xhstMaps").text(json.map_privacy);
//            $("#downvote_weight").val(json.weight*-100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};



// Update vote on slider timeout:
$('#downvote-slider').on('change', function(event){
    event.preventDefault();
    console.log("Change Detected!")  // sanity check
    get_downvote_value();
});

// AJAX for grabbing vote weight
function get_downvote_value() {
    console.log("Grabbing vote numbers") // sanity check
    console.log($('#id_downvote').val());
    $.ajax({
        url : "/votecalc/", // the endpoint
        type : "POST", // http method
        data : { weight : $('#id_downvote').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#downweight").text(json.weight+"% VOTE = ");
            $("#downvalue").text("-"+json.weightvalue+" SBD");
            $("#downvote_weight").val(json.weight*-100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};
         20190304100928J  #      A       
                                      / /   u s i n g   j Q u e r y 
 f u n c t i o n   g e t C o o k i e ( n a m e )   { 
         v a r   c o o k i e V a l u e   =   n u l l ; 
         i f   ( d o c u m e n t . c o o k i e   & &   d o c u m e n t . c o o k i e   ! = =   ' ' )   { 
                 v a r   c o o k i e s   =   d o c u m e n t . c o o k i e . s p l i t ( ' ; ' ) ; 
                 f o r   ( v a r   i   =   0 ;   i   <   c o o k i e s . l e n g t h ;   i + + )   { 
                         v a r   c o o k i e   =   j Q u e r y . t r i m ( c o o k i e s [ i ] ) ; 
                         / /   D o e s   t h i s   c o o k i e   s t r i n g   b e g i n   w i t h   t h e   n a m e   w e   w a n t ? 
                         i f   ( c o o k i e . s u b s t r i n g ( 0 ,   n a m e . l e n g t h   +   1 )   = = =   ( n a m e   +   ' = ' ) )   { 
                                 c o o k i e V a l u e   =       d e c o d e U R I C o m p o n e n t ( c o o k i e . s u b s t r i n g ( n a m e . l e n g t h   +   1 ) ) ; 
                                 b r e a k ; 
                         } 
                 } 
         } 
         r e t u r n   c o o k i e V a l u e ; 
 } 
 v a r   c s r f t o k e n   =   g e t C o o k i e ( ' c s r f t o k e n ' ) ; 
 f u n c t i o n   c s r f S a f e M e t h o d ( m e t h o d )   { 
         / /   t h e s e   H T T P   m e t h o d s   d o   n o t   r e q u i r e   C S R F   p r o t e c t i o n 
         r e t u r n   ( / ^ ( G E T | H E A D | O P T I O N S | T R A C E ) $ / . t e s t ( m e t h o d ) ) ; 
 } 
 $ . a j a x S e t u p ( { 
         b e f o r e S e n d :   f u n c t i o n ( x h r ,   s e t t i n g s )   { 
                 i f   ( ! c s r f S a f e M e t h o d ( s e t t i n g s . t y p e )   & &   ! t h i s . c r o s s D o m a i n )   { 
                         x h r . s e t R e q u e s t H e a d e r ( " X - C S R F T o k e n " ,   c s r f t o k e n ) ; 
                 } 
         } 
 } ) ; 
 
 / /   C o m m e n t F o r m 
 
 / /   U p d a t e   v o t e   o n   s l i d e r   t i m e o u t : 
 $ ( ' # c o m m e n t e r ' ) . o n ( ' c h a n g e ' ,   f u n c t i o n ( e v e n t ) { 
         e v e n t . p r e v e n t D e f a u l t ( ) ; 
         c o n s o l e . l o g ( " C o m m e n t   I n p u t   D e t e c t e d ! " )     / /   s a n i t y   c h e c k 
         $ ( " # p o s t _ b o d y " ) . v a l ( $ ( ' # c o m m e n t e r ' ) [ 0 ] [ ' e l e m e n t s ' ] [ 1 ] [ ' v a l u e ' ] ) ; 
 / /         u p d a t e _ c o m m e n t _ b o d y ( ) ; 
 } ) ; 
 
 / /   S l i d e r F o r m s 
 
 / /   U p d a t e   v o t e   o n   s l i d e r   t i m e o u t : 
 $ ( ' # u p v o t e - s l i d e r ' ) . o n ( ' c h a n g e ' ,   f u n c t i o n ( e v e n t ) { 
         e v e n t . p r e v e n t D e f a u l t ( ) ; 
         c o n s o l e . l o g ( " C h a n g e   D e t e c t e d ! " )     / /   s a n i t y   c h e c k 
         g e t _ u p v o t e _ v a l u e ( ) ; 
 } ) ; 
 
 / /   A J A X   f o r   g r a b b i n g   v o t e   w e i g h t 
 f u n c t i o n   g e t _ u p v o t e _ v a l u e ( )   { 
         c o n s o l e . l o g ( " G r a b b i n g   v o t e   n u m b e r s " )   / /   s a n i t y   c h e c k 
         c o n s o l e . l o g ( $ ( ' # i d _ u p v o t e ' ) . v a l ( ) ) ; 
         $ . a j a x ( { 
                 u r l   :   " / v o t e c a l c / " ,   / /   t h e   e n d p o i n t 
                 t y p e   :   " P O S T " ,   / /   h t t p   m e t h o d 
                 d a t a   :   {   w e i g h t   :   $ ( ' # i d _ u p v o t e ' ) . v a l ( )   } ,   / /   d a t a   s e n t   w i t h   t h e   p o s t   r e q u e s t 
 
                 / /   h a n d l e   a   s u c c e s s f u l   r e s p o n s e 
                 s u c c e s s   :   f u n c t i o n ( j s o n )   { 
                         c o n s o l e . l o g ( j s o n ) ;   / /   l o g   t h e   r e t u r n e d   j s o n   t o   t h e   c o n s o l e 
                         $ ( " # u p w e i g h t " ) . t e x t ( j s o n . w e i g h t + " %   V O T E   =   " ) ; 
                         $ ( " # u p v a l u e " ) . t e x t ( j s o n . w e i g h t v a l u e + "   S B D " ) ; 
                         $ ( " # u p v o t e _ w e i g h t " ) . v a l ( j s o n . w e i g h t * 1 0 0 ) ; 
                         c o n s o l e . l o g ( " s u c c e s s " ) ;   / /   a n o t h e r   s a n i t y   c h e c k 
                 } , 
 
                 / /   h a n d l e   a   n o n - s u c c e s s f u l   r e s p o n s e 
                 e r r o r   :   f u n c t i o n ( x h r , e r r m s g , e r r )   { 
                         $ ( ' # r e s u l t s ' ) . h t m l ( " < d i v   c l a s s = ' a l e r t - b o x   a l e r t   r a d i u s '   d a t a - a l e r t > O o p s !   W e   h a v e   e n c o u n t e r e d   a n   e r r o r :   " + e r r m s g + 
                                 "   < a   h r e f = ' # '   c l a s s = ' c l o s e ' > & t i m e s ; < / a > < / d i v > " ) ;   / /   a d d   t h e   e r r o r   t o   t h e   d o m 
                         c o n s o l e . l o g ( x h r . s t a t u s   +   " :   "   +   x h r . r e s p o n s e T e x t ) ;   / /   p r o v i d e   a   b i t   m o r e   i n f o   a b o u t   t h e   e r r o r   t o   t h e   c o n s o l e 
                 } 
         } ) ; 
 } ; 
 
 / /   U p d a t e   v o t e   o n   s l i d e r   t i m e o u t : 
 $ ( ' # d a s h S u m m a r y ' ) . o n ( ' c l i c k ' ,   f u n c t i o n ( e v e n t ) { 
         e v e n t . p r e v e n t D e f a u l t ( ) ; 
         c o n s o l e . l o g ( " D a s h b o a r d   S u m m a r y   R e q u e s t e d ! " ) ;     / /   s a n i t y   c h e c k 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l S u m m a r y " ) . s t y l e . d i s p l a y = " b l o c k " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s u m m a r y F e e d " ) . s t y l e . d i s p l a y = " b l o c k " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l Y e a r s " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # a c t i v i t y D e t a i l " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l S e t t i n g s " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # x h s t S e t t i n g s " ) . s t y l e . d i s p l a y = " n o n e " ; 
 } ) ; 
 
 / /   U p d a t e   v o t e   o n   s l i d e r   t i m e o u t : 
 $ ( ' # d a s h A c t i v i t i e s ' ) . o n ( ' c l i c k ' ,   f u n c t i o n ( e v e n t ) { 
         e v e n t . p r e v e n t D e f a u l t ( ) ; 
         c o n s o l e . l o g ( " D a s h b o a r d   A c t i v i t i e s   R e q u e s t e d ! " )     / /   s a n i t y   c h e c k 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l S u m m a r y " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s u m m a r y F e e d " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l S e t t i n g s " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # x h s t S e t t i n g s " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l Y e a r s " ) . s t y l e . d i s p l a y = " b l o c k " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # a c t i v i t y D e t a i l " ) . s t y l e . d i s p l a y = " b l o c k " ; 
 } ) ; 
 
 / /   D i s p l a y   a n d   E d i t   U s e r s   X H S T   S e t t i n g s   ( A P I   K e y ) 
 $ ( ' # d a s h S e t t i n g s ' ) . o n ( ' c l i c k ' ,   f u n c t i o n ( e v e n t ) { 
         e v e n t . p r e v e n t D e f a u l t ( ) ; 
         c o n s o l e . l o g ( " U s e r   S e t t i n g s   R e q u e s t e d ! " )   / /   s a n i t y   c h e c k 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l S u m m a r y " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s u m m a r y F e e d " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l Y e a r s " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # a c t i v i t y D e t a i l " ) . s t y l e . d i s p l a y = " n o n e " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # s i d e p a n e l S e t t i n g s " ) . s t y l e . d i s p l a y = " b l o c k " ; 
         d o c u m e n t . q u e r y S e l e c t o r ( " # x h s t S e t t i n g s " ) . s t y l e . d i s p l a y = " b l o c k " ; 
         g e t _ u s e r _ s e t t i n g s ( ) ; 
 } ) ; 
 
 / /   A J A X   f o r   g r a b b i n g   v o t e   w e i g h t 
 f u n c t i o n   g e t _ u s e r _ s e t t i n g s ( )   { 
         c o n s o l e . l o g ( " R e t r i e v i n g   S e t t i n g s " )   / /   s a n i t y   c h e c k 
         $ . a j a x ( { 
                 u r l   :   " / u s e r s e t t i n g s / " ,   / /   t h e   e n d p o i n t 
                 t y p e   :   " G E T " ,   / /   h t t p   m e t h o d 
                 / /   h a n d l e   a   s u c c e s s f u l   r e s p o n s e 
                 s u c c e s s   :   f u n c t i o n ( j s o n )   { 
                         c o n s o l e . l o g ( j s o n ) ;   / /   l o g   t h e   r e t u r n e d   j s o n   t o   t h e   c o n s o l e 
                         $ ( " # x h s t A P I K e y " ) . t e x t ( j s o n . a p i k e y ) ; 
                         $ ( " # x h s t M a p s " ) . t e x t ( j s o n . m a p _ p r i v a c y ) ; 
 / /                         $ ( " # d o w n v o t e _ w e i g h t " ) . v a l ( j s o n . w e i g h t * - 1 0 0 ) ; 
                         c o n s o l e . l o g ( " s u c c e s s " ) ;   / /   a n o t h e r   s a n i t y   c h e c k 
                 } , 
 
                 / /   h a n d l e   a   n o n - s u c c e s s f u l   r e s p o n s e 
                 e r r o r   :   f u n c t i o n ( x h r , e r r m s g , e r r )   { 
                         $ ( ' # r e s u l t s ' ) . h t m l ( " < d i v   c l a s s = ' a l e r t - b o x   a l e r t   r a d i u s '   d a t a - a l e r t > O o p s !   W e   h a v e   e n c o u n t e r e d   a n   e r r o r :   " + e r r m s g + 
                                 "   < a   h r e f = ' # '   c l a s s = ' c l o s e ' > & t i m e s ; < / a > < / d i v > " ) ;   / /   a d d   t h e   e r r o r   t o   t h e   d o m 
                         c o n s o l e . l o g ( x h r . s t a t u s   +   " :   "   +   x h r . r e s p o n s e T e x t ) ;   / /   p r o v i d e   a   b i t   m o r e   i n f o   a b o u t   t h e   e r r o r   t o   t h e   c o n s o l e 
                 } 
         } ) ; 
 } ; 
 
 
 
 / /   U p d a t e   v o t e   o n   s l i d e r   t i m e o u t : 
 $ ( ' # d o w n v o t e - s l i d e r ' ) . o n ( ' c h a n g e ' ,   f u n c t i o n ( e v e n t ) { 
         e v e n t . p r e v e n t D e f a u l t ( ) ; 
         c o n s o l e . l o g ( " C h a n g e   D e t e c t e d ! " )     / /   s a n i t y   c h e c k 
         g e t _ d o w n v o t e _ v a l u e ( ) ; 
 } ) ; 
 
 / /   A J A X   f o r   g r a b b i n g   v o t e   w e i g h t 
 f u n c t i o n   g e t _ d o w n v o t e _ v a l u e ( )   { 
         c o n s o l e . l o g ( " G r a b b i n g   v o t e   n u m b e r s " )   / /   s a n i t y   c h e c k 
         c o n s o l e . l o g ( $ ( ' # i d _ d o w n v o t e ' ) . v a l ( ) ) ; 
         $ . a j a x ( { 
                 u r l   :   " / v o t e c a l c / " ,   / /   t h e   e n d p o i n t 
                 t y p e   :   " P O S T " ,   / /   h t t p   m e t h o d 
                 d a t a   :   {   w e i g h t   :   $ ( ' # i d _ d o w n v o t e ' ) . v a l ( )   } ,   / /   d a t a   s e n t   w i t h   t h e   p o s t   r e q u e s t 
 
                 / /   h a n d l e   a   s u c c e s s f u l   r e s p o n s e 
                 s u c c e s s   :   f u n c t i o n ( j s o n )   { 
                         c o n s o l e . l o g ( j s o n ) ;   / /   l o g   t h e   r e t u r n e d   j s o n   t o   t h e   c o n s o l e 
                         $ ( " # d o w n w e i g h t " ) . t e x t ( j s o n . w e i g h t + " %   V O T E   =   " ) ; 
                         $ ( " # d o w n v a l u e " ) . t e x t ( " - " + j s o n . w e i g h t v a l u e + "   S B D " ) ; 
                         $ ( " # d o w n v o t e _ w e i g h t " ) . v a l ( j s o n . w e i g h t * - 1 0 0 ) ; 
                         c o n s o l e . l o g ( " s u c c e s s " ) ;   / /   a n o t h e r   s a n i t y   c h e c k 
                 } , 
 
                 / /   h a n d l e   a   n o n - s u c c e s s f u l   r e s p o n s e 
                 e r r o r   :   f u n c t i o n ( x h r , e r r m s g , e r r )   { 
                         $ ( ' # r e s u l t s ' ) . h t m l ( " < d i v   c l a s s = ' a l e r t - b o x   a l e r t   r a d i u s '   d a t a - a l e r t > O o p s !   W e   h a v e   e n c o u n t e r e d   a n   e r r o r :   " + e r r m s g + 
                                 "   < a   h r e f = ' # '   c l a s s = ' c l o s e ' > & t i m e s ; < / a > < / d i v > " ) ;   / /   a d d   t h e   e r r o r   t o   t h e   d o m 
                         c o n s o l e . l o g ( x h r . s t a t u s   +   " :   "   +   x h r . r e s p o n s e T e x t ) ;   / /   p r o v i d e   a   b i t   m o r e   i n f o   a b o u t   t h e   e r r o r   t o   t h e   c o n s o l e 
                 } 
         } ) ; 
 } ; 
