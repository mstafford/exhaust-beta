from activities.models import Activity, activityType
from django.contrib.auth.models import User

import json
import re

def sync_users():
    userfile = open('xhstusers.json','r')
    userdata = json.loads(userfile.read())
    for user in userdata:
        print(user['fields']['username'])
        if user['fields']['username'] == 'exhaustAdmin':
            print("Admin account detected! Skipping!")
            continue
        User.objects.get_or_create(username=user['fields']['username'])

def print_users():
    users = User.objects.all()
    for user in users:
        print(user.username)

def sync_acts():
    actfile = open('xhstacts.json', 'r')
    actdata = json.loads(actfile.read())
    atypes = ['null','running','cycling','hiking','climbing','yoga','strength']
    for act in actdata:
        if act['fields']['posted'] != True:
            continue
        else:
             username = re.findall("@.+/",act['fields']['permlink'])
             username = re.sub("@","",username[0])
             username = re.sub("/","",username)
             print(username)
             print(act['fields']['type'])
             user = User.objects.get(username=username)
             atype = activityType.objects.get(id=int(act['fields']['type']))
             activity = Activity.objects.get_or_create(user=user, type=atype, permlink=act['fields']['permlink'])
             activity[0].ascent = act['fields']['ascent']
             activity[0].climb_data = act['fields']['climb_data']
             activity[0].comments = act['fields']['comments']
             activity[0].coach = act['fields']['coach']
             activity[0].descent = act['fields']['descent']
             activity[0].distance = act['fields']['distance']
             activity[0].duration = act['fields']['duration']
             activity[0].equipment = act['fields']['equipment']
             activity[0].gpsfile = act['fields']['gpsfile']
             activity[0].muscles = act['fields']['muscles']
             activity[0].pace = act['fields']['pace']
             activity[0].permlink = act['fields']['permlink']
             activity[0].photo= act['fields']['photo']
             activity[0].photo2 = act['fields']['photo2']
             activity[0].photo3 = act['fields']['photo3']
             activity[0].postdate = act['fields']['postdate']
             activity[0].posted = act['fields']['posted']
             activity[0].rewards = act['fields']['rewards']
             activity[0].speed = act['fields']['speed']
             activity[0].steps = act['fields']['steps']
             activity[0].start = act['fields']['start']
             activity[0].strength_data = act['fields']['strength_data']
             activity[0].style = act['fields']['style']
             activity[0].tags = act['fields']['tags']
             activity[0].title = act['fields']['title']
             activity[0].uploaded_at = act['fields']['uploaded_at']
             activity[0].verb = act['fields']['verb']
             activity[0].video = act['fields']['video']
             activity[0].save()

