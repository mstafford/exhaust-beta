// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue =   decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

// Toggler for Manual New Activity Method Selection:
$('#act_delete').on('click', function(event){
    event.preventDefault();
    console.log("Activity Deletion Request Detected!!");  // sanity check
    $.ajax({
        url : "/delete/", // the endpoint
        type : "POST", // http method
        data : { activity : $('#activity_id').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
            window.location.href = "https://xhaust.me/upload/";
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });

});
// Toggler for Manual New Activity Method Selection:
$('#act_update').on('click', function(event){
    event.preventDefault();
    console.log("Activity Update Request Detected!!");  // sanity check
});

// Toggler for Manual New Activity Method Selection:
$('#act_preview').on('click', function(event){
    event.preventDefault();
    console.log("Post Preview Request Detected!!");  // sanity check
});


// Update vote on slider timeout:
$('#commenter').on('change', function(event){
    event.preventDefault();
    console.log("Comment Input Detected!")  // sanity check
    var text = $('#id_commentbody').val();
//    var regex = /\r?\n|\r/;
//    text = text.replace(regex, "\n");
    $("#comment_body").val(text);
//    update_comment_body();
});

// SliderForms

// Update vote on slider timeout:
$('#upvote-slider').on('change', function(event){
    event.preventDefault();
    console.log("Change Detected!")  // sanity check
    get_upvote_value();
});

// AJAX for grabbing vote weight
function get_upvote_value() {
    console.log("Grabbing vote numbers") // sanity check
    console.log($('#id_upvote').val());
    $.ajax({
        url : "/votecalc/", // the endpoint
        type : "POST", // http method
        data : { weight : $('#id_upvote').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#upweight").text(json.weight+"% VOTE = ");
            $("#upvalue").text(json.weightvalue+" SBD");
            $("#upvote_weight").val(json.weight*100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

// Toggler for Manual New Activity Method Selection:
$('#togglemanual').on('click', function(event){
    event.preventDefault();
    console.log("Manual Data Entry Selected!");  // sanity check
    document.querySelector("#manualentry").style.display="flex";
    document.querySelector("#gpsentry").style.display="none";
    document.querySelector("#acctsync").style.display="none";
});

// Toggler for Manual New Activity Method Selection:
$('#togglefile').on('click', function(event){
    event.preventDefault();
    console.log("GPS Upload Selected!");  // sanity check
    document.querySelector("#manualentry").style.display="none";
    document.querySelector("#gpsentry").style.display="flex";
    document.querySelector("#acctsync").style.display="none";
});

// Toggler for Manual New Activity Method Selection:
$('#togglesync').on('click', function(event){
    event.preventDefault();
    console.log("AccountSync Selected!");  // sanity check
    document.querySelector("#manualentry").style.display="none";
    document.querySelector("#gpsentry").style.display="none";
    document.querySelector("#acctsync").style.display="flex";
});

// Update vote on slider timeout:
$('#dashSummary').on('click', function(event){
    event.preventDefault();
    console.log("Dashboard Summary Requested!");  // sanity check
    document.querySelector("#sidepanelSummary").style.display="block";
    document.querySelector("#summaryFeed").style.display="block";
    document.querySelector("#sidepanelYears").style.display="none";
    document.querySelector("#activityDetail").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="none";
    document.querySelector("#xhstSettings").style.display="none";
});

// Update vote on slider timeout:
$('#dashActivities').on('click', function(event){
    event.preventDefault();
    console.log("Dashboard Activities Requested!")  // sanity check
    document.querySelector("#sidepanelSummary").style.display="none";
    document.querySelector("#summaryFeed").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="none";
    document.querySelector("#xhstSettings").style.display="none";
    document.querySelector("#sidepanelYears").style.display="block";
    document.querySelector("#activityDetail").style.display="block";
});

// Display and Edit Users XHST Settings (API Key)
$('#dashSettings').on('click', function(event){
    event.preventDefault();
    console.log("User Settings Requested!") // sanity check
    document.querySelector("#sidepanelSummary").style.display="none";
    document.querySelector("#summaryFeed").style.display="none";
    document.querySelector("#sidepanelYears").style.display="none";
    document.querySelector("#activityDetail").style.display="none";
    document.querySelector("#sidepanelSettings").style.display="block";
    document.querySelector("#xhstSettings").style.display="block";
    get_user_settings();
});

// AJAX for grabbing vote weight
function get_user_settings() {
    console.log("Retrieving Settings") // sanity check
    $.ajax({
        url : "/usersettings/", // the endpoint
        type : "GET", // http method
        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#xhstAPIKey").text(json.apikey);
            $("#xhstMaps").text(json.map_privacy);
//            $("#downvote_weight").val(json.weight*-100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};



// Update vote on slider timeout:
$('#downvote-slider').on('change', function(event){
    event.preventDefault();
    console.log("Change Detected!")  // sanity check
    get_downvote_value();
});

// AJAX for grabbing vote weight
function get_downvote_value() {
    console.log("Grabbing vote numbers") // sanity check
    console.log($('#id_downvote').val());
    $.ajax({
        url : "/votecalc/", // the endpoint
        type : "POST", // http method
        data : { weight : $('#id_downvote').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // log the returned json to the console
            $("#downweight").text(json.weight+"% VOTE = ");
            $("#downvalue").text("-"+json.weightvalue+" SBD");
            $("#downvote_weight").val(json.weight*-100);
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};
